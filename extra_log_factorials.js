`use strict`
// https://www.hackerrank.com/challenges/extra-long-factorials/problem

function factorial(n) {
    // we can't handle very large numbers with just regular int value
    let sum = convertNumberToArray(1);
    for(let i = 2; i <= n; i++) {
        sum = multiply(sum, convertNumberToArray(i));
    }
    // remove all the leading zeros we used to make the arrays the same size
    let value = sum[0];
    while(value == 0) {
        sum.shift()
        value = sum[0];
    }
    return sum.join("");
}

// simple function to convert a number into an array 10 -> [1,0]
function convertNumberToArray(n) {
    if(n == 0) return [0];
    let output = [];
    let value = 0;
    while(n > 0) {
        value = n % 10;
        output.unshift(value);
        n = Math.floor(n / 10);
    }
    return output;
}

// just returns an array n values of zero
function appendZeros(n) {
    let output = [];
    for(let i = 0; i < n; i ++) {
        output.push(0);
    }
    return output;
}

// this function just adds zeros to the front of an array until the size matches
function prependZeros(arr, size) {
    for(let i = arr.length; i <= size; i++) {
        arr.unshift(0);
    }
    return arr
}

// this function will multiply 2 arrays, the arrays present an integer number, the first number should be the larger one
// just like grade school math we work from the bottom row backwards multiplying by each number from the top row
/*
   [2][3]
X  [1][2]
--------
[2][7][6]
*/
function multiply(arr1, arr2) {
    let output = [];
    let carry = 0;
    let subtotal = [];
    let maxArraySize = 0;
    for(let i = arr2.length - 1; i >= 0; i--) {
        // need to append zeros when we go down each value
        subtotal = appendZeros((arr2.length - 1) - i);
        let value = carry;
        carry = 0;
        for(let j = arr1.length -1; j >=0; j--) {
            value =arr2[i] * arr1[j] + carry;
            carry = 0;
            if(value >= 10) {
                carry = Math.floor(value / 10);
                value =  value % 10;
            }
            subtotal.unshift(value);
        }
        if(subtotal.length > maxArraySize) {
            maxArraySize = subtotal.length;
        }
        // if there is a carry flush it to the front
        if(carry > 0) subtotal.unshift(carry);
        output.push(subtotal);
    }
    // when we have all our outputs we must normalize them into same size arrays
    for(let i = 0; i < output.length; i++) {
        output[i] = prependZeros(output[i], maxArraySize);
    }

    return add(output);
}

// this function will add arrays at their respective index, all arrays should be the same size
/*
  [1][2][3]
+ [0][3][9]
----------
  [1][6][2]
*/
function add(numbers) {
    let carry = 0;
    let width = numbers[0].length - 1;
    let output = [];
    // start top down but go in reverse when adding
    for(let j = width; j >= 0; j--) {
        let sum = carry;
        carry = 0;
        for(let i = 0; i < numbers.length; i++) {
            sum = sum + numbers[i][j]
            // if we are over 10 then we need to carry
            if(sum >= 10) {
                carry++
                sum = sum % 10
            }
        }
        output.unshift(sum);
    } 
    return output; 
}

let addnumbers = [[0,6,0,0], [3,6,0,0]];
// console.log(add(addnumbers));

let multiply1 = [2,4];
let multiply2 = [5];
// console.log(multiply(multiply1, multiply2));

// console.log(convertNumberToArray(100));

console.log(factorial(25));