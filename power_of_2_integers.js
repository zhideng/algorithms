// https://www.interviewbit.com/problems/power-of-two-integers/

function isPowerOf(n) {
    let val = false;

    // so we need to know if the number that is given can be expressed in A^P
    // we need to continueously try all powers until the output is larger than n
    // we need to increase our start value until at max it's sq rot of n because that would be the min value it could be
    let maxVal = Math.sqrt(n);

    for (let i = 2; i <= maxVal; i++) {
        let sqCounter = 1;
        let sqValue = Math.pow(i, sqCounter);
        while(sqValue <= n) {
            if(sqValue == n) {
                return true;
             }
             sqCounter++;
             sqValue = Math.pow(i, sqCounter);
        }
    }

    return false;
}

console.log(isPowerOf(16));