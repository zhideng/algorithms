// https://www.interviewbit.com/problems/add-one-to-number/

function add_one(A) {
    let counter = 1;
    let sum = A[A.length - counter] + 1;
    let carry = 0;
    if (sum >= 10) {
        carry = Math.floor(sum / 10);
        sum = sum % 10;
    }
    A[A.length - counter] = sum;
    counter++;
    while (carry > 0 && counter <= A.length) {
        sum = A[A.length - counter] + carry;
        carry = 0;
        if (sum >= 10) {
            carry = Math.floor(sum / 10);
            sum = sum % 10;
        }
        A[A.length - counter] = sum;
        counter++;
    }
    // check if there is extra carry
    if(carry > 0) {
        A.unshift(carry);
    }
    // have to remove leading zeros...
    while(A[0] == 0) {
        A.shift();
    }
    return A;
}

let numbers = [0, 3, 7, 6, 4, 0, 5, 5, 5]
console.log(add_one(numbers));
