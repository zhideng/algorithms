// https://stackoverflow.com/questions/2680548/given-an-array-of-numbers-return-array-of-products-of-all-other-numbers-no-div
// sample question from email
// https://leetcode.com/problems/product-of-array-except-self/

// not allowed to use division which sucks cause that would yield O(n) easily
function products(A) {
    // initialize the array to all 1s
    let output = new Array(A.length);
    for(let i = 0; i < output.length; i++) {
        output[i] = 1;
    }

    // go through each item in the array and multiply by each output array except when the index is the same
    for(let i = 0; i < output.length; i++) {
        for(let k = 0; k < A.length; k++) {
            if(i != k) {
                output[i] = output[i] * A[k];
            }
        }
    }

    return output;
}

// the simple one is n^2 which is no good performance, so getting some help from the internet we can accomplish the
// same by creating 2 arrays one that contains all products to the left of the current index and one all products to the
// right of the current index then multiplying the respective index values in both arrays

function products_faster(A) {
    // create left product array
    let L = new Array(A.length);

    // left products
    let product = 1
    for(let i = 0; i < A.length; i++) {
        L[i] = product;
        product = product * A[i];
    }

    // create right product array
    let R = new Array(A.length);

    // right products
    product = 1;
    for(let k = A.length - 1; k >= 0; k--) {
        R[k] = product;
        product = product * A[k];
    }

    // combine them into output, pick either array to mutate
    for(let j = 0; j < A.length; j++) {
        L[j] = L[j] * R[j];
    }

    return L;
}

// pretty much the same as the one above but don't use temp array
function products_lessSpace(A) {
    let answer = new Array(A.length);

    let product = 1;
    for(let i = 0; i < A.length; i++) {
        answer[i] = product;
        product = product * A[i];
    }

    // now go the other way 
    product = 1
    for(let j = A.length - 1; j >= 0; j--) {
        answer[j] = product * answer[j];
        product = product * A[j];
    }

    return answer;
}


let values = [1, 2, 3, 4, 5]

console.log(products_lessSpace(values));