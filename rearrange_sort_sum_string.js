// https://www.geeksforgeeks.org/rearrange-a-string-in-sorted-order-followed-by-the-integer-sum/

function sum_sort(val) {
    let alphas = [];
    let numbers = [];
    let values = val.split("");
    for(let i = 0; i < values.length; i++) {
        if(isNaN(values[i])) {
            alphas.push(values[i]);
        } else {
            numbers.push(Number.parseInt(values[i]));
        }
    }
    // sort the alphas
    let sorted = alphas.sort();

    // sum the numbers
    let sum = numbers.reduce((previous, current) => {
        return previous += current;
    });

    return sorted.join("") + sum;
}

console.log(sum_sort("ACCBA10D2EW30"));