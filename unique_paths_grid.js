// https://www.interviewbit.com/problems/grid-unique-paths/

function find_path(a,b, pos_a, pos_b) {
    let count = 0;
    if(pos_a == a - 1 && pos_b == b - 1) {
        return 1;
    }

    if(pos_a + 1 < a){
        count += find_path(a,b, pos_a + 1, pos_b);
    }       
    
    if(pos_b + 1 < b) {
        count += find_path(a,b, pos_a, pos_b + 1);
    }
    
    return count;
}

function unique_paths(a, b) {
   let count = 0;
    
   count += find_path(a, b, 0, 0);

   return count;
}

console.log(unique_paths(3,3))