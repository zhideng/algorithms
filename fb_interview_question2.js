A = [0 4 0 0 0 2 0 .. 0 -7 0 ... 0 5 9 0 .. 0]
B = [5 0 0 0 6 7 0 .. 9 3 0 .... 0 0 0 .....0]

dot(A,B) = A0*B0 + A1*B1 + ... + An*Bn

// Big O(n)
function dot(A, B) {
  
  if(A.length != B.length) {
    return throw new Error("Arrays not equal length"); 
  }
  
  let n = A.length;
  let sum = 0;
  
  for(let i = 0; i < n; i++){
    sum += A[i]*B[i];
  }
  
  return sum;
  
}

A ={
  "1": 4
  "5": 2
}

B = {
  "0": 5
  "4": 6
  "5": 7
}

function dot2(A, B) {
  
  if(A.keys().length == 0 || B.keys().length == 0) {
    return 0; 
  }
 
  let indexA = 0;
  let indexB = 0;
  let a_keys = A.keys();
  let b_keys = B.keys();
  let sum = 0;

  while(indexA < a_keys.length && indexB < b_keys.length) {
    let a_value = a_keys[indexA];
    let b_value = b_keys[indexB];
    if(a_value == b_value) {
      sum += A[a_value] * B[b_value];
      indexA++;
      indexB++;
    } else {
      if(a_value < b_value) {
        indexA++;
      } else {
        indexB++;
      }
    }
    
  }
  
  return sum;
  
}

function dot3(A, B) {
  
  if(A.keys().length == 0 || B.keys().length == 0) {
    return 0;
  }
  
  let smallerList;
  let largerList;
  
  if(A.length < B.lebgth) {
    smallerList =A.keys();
    largerList = B.keys();
  } else {
    smallerList = B.keys();
    largerList = A.keys();
  }
  
  let sum = 0;
  for(let i = 0; i < smallerList.length; i++) {
     if(largerList[smallerList[i]] != null) {
        sum += smallerList[i] * largerList[smallerList[i]]; 
     }
  }
  
  return sum;
  
}


A =[ (1, 4), (3, 7) ]

B = [ (0, 5), (4, 6), (5, 7)]  

