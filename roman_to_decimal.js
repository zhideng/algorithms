// https://www.geeksforgeeks.org/converting-roman-numerals-decimal-lying-1-3999/

function roman_to_decimal(val) {
    // we need a mapping of roman to decimals
    let mapping = {
        "I": 1,
        "V": 5,
        "X": 10,
        "L": 50,
        "C": 100,
        "D": 500,
        "M": 1000
    }

    let letters = val.split("");
    let previousValue = mapping[letters[0]];
    let total = 0;
    let subtotal = mapping[letters[0]];
    for(let i = 1; i < letters.length; i++) {
        // get the matching value from the mapping
        let value = mapping[letters[i]];
        if(value < previousValue) {
            // flush to total
            total += subtotal
            // reset the subtotal
            subtotal = value;
            previousValue = value;
        } else if(value > previousValue) {
            // if the value is greater than the previous it means we have a subtraction state
            subtotal = value - subtotal;
            // flush the subtotal
            total += subtotal;
            // assign previous value
            previousValue = value;
            // reset subtotal
            subtotal = 0;
        } else {
            // the values are equal so add to subtotal
            subtotal += value;
        }
    }
    // flush any subtotals
    if(subtotal > 0) total += subtotal;
    return total;
}

console.log(roman_to_decimal("MCMLXXXIV"));