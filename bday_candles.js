// https://www.hackerrank.com/challenges/birthday-cake-candles/problem

function how_many(arr) {
    // if we don't sort the array we can do O(n) by traversing and storing the values that are the largest values
    let count = 0;
    let maxHeight = 0;
    for(let i = 0; i < arr.length; i++) {
        if(maxHeight < arr[i]) {
            // new maxheight found assign and reset count
            maxHeight = arr[i];
            count = 1;
        } else if (maxHeight == arr[i]) {
            count++;
        }
    }
    return count;
}

console.log(how_many([4,2,1,3]));