// https://www.geeksforgeeks.org/look-and-say-sequence/

function number2Array(number) {
    if (number == 0 || number == 1) return [number];
    let arr = [];
    while (number >= 1) {
        arr.unshift(Number.parseInt(number % 10));
        number = number / 10;
    }
    return arr;
}

function look_and_say(n) {

    if (n == 1) return 1;

    // initialize the look and say with one value
    let lookup = [1];
    for (let i = 1; i < n; i++) {
        if (lookup.length >= n) {
            return lookup[n];
        } else {
            // generate the values for each value until n
            let numbers = number2Array(lookup[i - 1]);
            let counter = 0;
            let current;
            let value = "";
            for (j = 0; j < numbers.length; j++) {
                if (j == 0) {
                    current = numbers[j];
                    counter++;
                } else {
                    if (numbers[j] == current) {
                        // if the current number is the same as the prev just increment counter
                        counter++
                    } else {
                        value += counter;
                        value += current;
                        counter = 1;
                        current = numbers[j];
                    }
                }
            }
            // flush final output
            value += counter;
            value += current;
            lookup.push(value);
        }
    }
    return lookup[lookup.length - 1]
}
// console.log(look_and_say(3));
console.log(look_and_say(8));