// https://www.geeksforgeeks.org/largest-sum-subarray-least-k-numbers/

function largest_sum(A, k) {
    let values = []; // these are the current values
    let start_index = 0;
    let end_index = 0;
    let max;
    let max_start = 0;
    let max_end = 0;

    for(let i = 0; i < A.length; i++) {
        if(values.length < k && !values.includes(A[i])) {
            // we do not have enough values yet
            values.push(A[i]);
        } else if(values.length == k && !values.includes(A[i])) {
            // too many values we need to shift the window
            // start_index += 1;
            start_index = find_start_index(A, i, k);
            let item = values.shift(); // remove the first item in values list
            // move the start index 
            values.push(A[i]);
        }
        end_index = i;
        // calculate the max
        let value = sum_items(A, start_index, end_index);
        if(max == undefined || value > max) {
            // if this is the new max then assign it to the index values
            max = value;
            max_start = start_index
            max_end = end_index;
        }
    }
    return max;
    // return sum_items(A, max_start, max_end);
}

function sum_items(A, start, end) {
    let sum = 0;
    for(let i = start; i <= end; i ++) {
        sum += A[i];
    }
    return sum;
}

function find_start_index(A, end, k) {
    let values = [];
    let index = end;
    // go backwards until we have k unique values
    for(let i = end; i >= 0; i--) {
        if(values.length < k || values.includes(A[i])) {
            values.push(A[i]);
            index = i;
        } else {
            break;
        }
    }
    return index;
}

// let values = [-4, -2, 1, -3]

let values = [1, 2, 1, 5, 0, 1]

// let values = [1, 1, 1, 5, 1, 2]

console.log(largest_sum(values, 3));