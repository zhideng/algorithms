// https://practice.geeksforgeeks.org/problems/subarray-with-given-sum/0

function sub_array(arr, sum) {
    // start from the the beginning and keep adding the next item
    // once you match sum return or once you go over move to next starting item
    let currentSum = 0
    if(arr[0] == sum) return [1]  // if the value we are looking for is the first element
    for(let i = 0; i < arr.length; i++) {
        currentSum += arr[i]
        for(j=i+1; j < arr.length; j++) {
            currentSum += arr[j];
            if(currentSum == sum) {
                return [i+1,j+1];
            } else if (currentSum > sum) {
                currentSum = 0;
                break;
            }
        }
    }
    return [-1];
}

let array = [1,2,3,7,5]
console.log(sub_array(array, 12));
let array2 = [1,2,3,4,5,6,7,8,9,10];
console.log(sub_array(array2, 15));