// https://www.geeksforgeeks.org/inplace-rotate-square-matrix-by-90-degrees/
// https://www.geeksforgeeks.org/turn-an-image-by-90-degree/

function rotate_image(mtrx) {
    // matrx is a 2D NxM array that
    // create an output matrix hat is MxN
    let output = new Array(mtrx[0].length);
    for(let i = 0; i< mtrx.length; i++) {
        for(let j = 0; j<mtrx[i].length; j++) {
            if(!output[j]) {
                output[j] = []
            }
            output[j].unshift(mtrx[i][j]);
        }  
    }
    return output;
}

// this is going to rotate the matrix without an output matrix (inplace)
function rotate_matrix(arr) {
    let N = arr.length;
    // go through each of the sets of numbers that have to be moved
    for(let x = 0; x < N / 2; x++) {
        for(let y = x; y < N - x - 1; y++) {
            // save the starting one in temp
            let temp = arr[x][y];

            // move right to the top
            arr[x][y] = arr[y][N - 1 - x];

            // bottom to right
            arr[y][N - 1 - x] = arr[N - 1 -x][N - 1 -y];

            // left to bottom
            arr[N - 1 -x][N - 1 -y] = arr[N - 1 - y][x];

            // temp to left
            arr[N - 1 - y][x] = temp;
        }
    }

    //if there is an inner matrix we will then do the same for inner matrix
}

function print_matrix(mtrx) {
    for(let i = 0; i< mtrx.length; i++) {
        let line = '';
        for(let j = 0; j < mtrx[i].length; j++) {
            line += mtrx[i][j] + ' ';
        }
        console.log(line);
    }
}

let image = [
    ['*', '*', '*', '4', '*', '*', '*'],
    ['*', '*', '*', '3', '*', '*', '*'],
    ['*', '*', '*', '2', '*', '*', '*'],
    ['*', '*', '*', '1', '*', '*', '*']
]

print_matrix(image);

let new_image = rotate_image(image);

print_matrix(new_image);

// let matrix = [
//     [1,2,3],
//     [4,5,6],
//     [7,8,9]
// ]

// print_matrix(matrix);

// rotate_matrix(matrix);

// print_matrix(matrix);

let matrix2 = [
    [1,2,3,4],
    [5,6,7,8],
    [9,10,11,12],
    [13,14,15,16]
]

print_matrix(matrix2);

rotate_matrix(matrix2);

print_matrix(matrix2);