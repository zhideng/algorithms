// https://www.interviewbit.com/problems/add-two-numbers-as-lists/

// Definition for singly-linked list.
function Node(data) {
    this.data = data
    this.next = null
}

function add(A, B) {
    let carry = 0;
    let nodeA = A;
    let nodeB = B;
    // let nodes = [];
    // let output = new Node(0);
    let nextNode2;
    let output2;
    while (nodeA || nodeB) {
        let node = new Node(0);
        let a = nodeA ? nodeA.data : 0;
        let b = nodeB ? nodeB.data : 0;
        let sum = a + b + carry;
        carry = 0;  // reset the carry
        if (sum > 9) {
            // we need to increment the carry
            carry = Math.floor(sum / 10);
            sum = sum % 10
        }
        node.data = sum;
        // nodes.push(node);

        if(output2 === undefined) {
            output2 = node;
            nextNode2 = output2;
        } else {
            nextNode2.next = node;
            nextNode2 = nextNode2.next;
        }
        
        nodeA = nodeA ? nodeA.next : null;
        nodeB = nodeB ? nodeB.next : null;
    }
    // if there is a remaining carry we need to add it
    if(carry > 0) {
        // nodes.push(new Node(carry));
        nextNode2.next = new Node(carry);
    }
    // output.data = nodes[0].data;
    // let nextNode = output;
    // for(let i = 1; i < nodes.length; i++) {
    //     nextNode.next = nodes[i];
    //     nextNode = nextNode.next;
    // }
    return output2;
}

function printList(A) {
    let node = A;
    let value = "";
    while (node) {
        value += node.data;
        node = node.next;
    }
    console.log(value);
}

let node1 = new Node(2);
node1.next = new Node(5);
node1.next.next = new Node(3);

let node2 = new Node(5);
node2.next = new Node(6);
node2.next.next = new Node(4);

// let node1 = new Node(9);
// node1.next = new Node(9);
// node1.next.next = new Node(9);

// let node2 = new Node(1);

printList(add(node1, node2));