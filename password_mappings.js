// https://www.businessinsider.com/15-brain-bending-interview-questions-that-every-facebook-engineer-can-answer-2012-3?op=1#how-can-you-benefit-from-fraudulent-facebook-credits-2

let mappings = {
    'a': ['A', '@', '4'],
    'e': ['E', '#'],
}

let possible_passwords = [];

let password = 'facebook'.split("");

for (let i = 0; i < password.length; i++) {
    let possible_mappings = mappings[password[i]] || password[i];
    // if the possible password is empty it means its the first character so far
    if(possible_passwords.length == 0) {
        possible_passwords = possible_mappings;
    } else {
        // take all the possible passwords so far and append all the mappings to create a new combination
        let combinations = [];
        for(let j = 0; j < possible_passwords.length; j++) {
            for(let k = 0; k < possible_mappings.length; k++) {
                combinations.push(possible_passwords[j] + possible_mappings[k]);
            }
        }
        // reassign the possoble passwords with the new combos
        possible_passwords = combinations;
    }
}

console.log(possible_passwords);

// this one was take from here: https://www.glassdoor.com/Interview/Given-a-dictionary-based-simple-password-create-all-possible-special-character-passwords-based-on-a-provided-mapping-QTN_241643.htm

var map = {
    a: ['A', '@', '4'],
    e: ['E', '#']
};

function recur(pw) {

    //base case
    if (pw.length === 1) return map[pw] || pw;

    //otherwise
    var current = map[pw.charAt(0)],
        rest = recur(pw.slice(1)), //recursively get possible combinations for substring
        result = [],
        i, j, k, l;

    if (current) { // has map
        for (i = 0, j = current.length; i < j; i++) {
            for (k = 0, l = rest.length; k < l; k++) {
                result.push(current[i] + rest[k]);
            }
        }
    } else { // no map
        for (k = 0, l = rest.length; k < l; k++) {
            result.push(pw.charAt(0) + rest[k]);
        }
    }

    return result;

}

console.log(recur('facebook'));