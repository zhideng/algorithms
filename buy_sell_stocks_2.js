// https://www.interviewbit.com/problems/best-time-to-buy-and-sell-stocks-ii/

var transaction = function(index, action) {
    return {
        index: index,
        action: action
    }
}

function max_profit(A) {
    // we want to go through this in O(n) we will track the fluctuations
    // so like stock chart we want to buy at the valley and sell at the peaks
    let profit = 0;
    let buyStock = true;
    let last_transaction_index = 0;
    //before we go into the loop determine if we should be buying or selling right off the bat
    if(A[1] - A[0] > 0) {
        buyStock = false;
        profit = profit - A[0];
    }

    for(let i = 1; i < A.length; i++) {
        if(buyStock) {
            // this means the  graph starts trending down and we need to find the valley
            if(A[i] > A[i -1]) {
                // the graph has turned and we need to buy
                profit -= A[i - 1];
                buyStock = false;
                last_transaction_index = i - 1;
            }
        } else {
            // this means the graph starts trending up and we need to find the peak
            if(A[i] < A[i - 1]) {
                profit += A[i - 1];
                buyStock = true;
                last_transaction_index = i - 1;
            }
        }
    }

    // if our last transaction was a buy and the last price is in the list is higher than our buy price than make one last sell
    if(!buyStock && A[last_transaction_index] < A[A.length - 1]) {
        profit += A[A.length - 1]
    }

    return profit;
}

// let prices = [1,2,3];

// let prices = [7,1,5,3,6,4];

// let prices = [1,2,3,4,5];

// let prices = [7,6,4,3,1];

// let prices = [];

// let prices = [1,1];

// let prices = [1]

let prices = [2,1]

console.log(max_profit(prices));