// https://www.geeksforgeeks.org/count-pairs-difference-equal-k/

function getAllPairs_Simple(arr, k) {
    let output = [];
    let lookup = [];
    for (let i = 0; i < arr.length; i++) {
        for (j = i + 1; j < arr.length; j++) {
            let number = Math.abs(arr[i] - arr[j]);
            if (number == k) {
                // check that both numbers are not already used as a distinct pair
                if (!lookup.includes(arr[i]) && !lookup.includes(arr[j])) {
                    output.push([arr[i], arr[j]]);
                    lookup.push(arr[i]);
                    lookup.push(arr[j]);
                }
            }
        }
    }
    return output
}

// assumes sorted array
function remove_duplicates(arr) {
    let current = arr[0];
    for(let i = 1; i < arr.length; i++) {
        if(current != arr[i]) {
            current = arr[i]
        } else {
            // duplicate so remove it from arr
            arr.splice(i, 1);
        }
    }
}

// this function will attempt to find the value in the given array
function findValue(arr, start, end, value) {
    // there are probably ways to do a faster look up
    // the simple is O(n) and look through each item
    // binary search is faster because it will start from the middle and then go left or right depending on value
    if(end >= start) {
        let mid = start + Math.round((end - start) / 2)
        if(value == arr[mid]) {
            return mid;
        }
        if(value > arr[mid]) {
            return findValue(arr, (mid+1), end, value);
        } else {
            return findValue(arr, start, (mid-1), value);
        }
    }
    return -1;
}

function getAllPairs_Sorting(arr, k) {
    let output = [];
    // sort the array
    let sorted = arr.sort();
    
    // remove the duplicates
    remove_duplicates(sorted);

    // start from first element and try and find item[n] + k value in array
    for(let i = 0; i < arr.length; i++) {
        let match = findValue(arr, i+1, arr.length-1, arr[i] + k);
        if(match > 0) {
            output.push([arr[i], arr[match]]);
        }
    }

    return output;
}

let numbers = [1, 5, 3, 4, 2, 1];
let pairs = getAllPairs_Simple(numbers, 3);
console.log(pairs);

let pairs2 = getAllPairs_Sorting(numbers, 3);
console.log(pairs2);