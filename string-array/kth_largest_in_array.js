// https://www.programcreek.com/2014/05/leetcode-kth-largest-element-in-an-array-java/
const array1 = [3, 2, 1, 5, 6, 4]

function simple_k_sort(arr, k) {
    // sort the array the return the kth element from the rear
    let sorted = arr.sort()
    return sorted[sorted.length - k]
}

console.log(simple_k_sort(array1, 4))

function heap_k_largest(arr, k) {
    // use an array to of size k to keep track of items
    let items = new Array(k);
    for (let i = 0; i < arr.length; i++) {
        items = insertIntoArraySorted(items, arr[i]);
    }
    return items[items.length - 1];
}

function insertIntoArraySorted(arr, value) {
    let size = arr.length;
    for (let i = 0; i < arr.length; i++) {
        // the value is larger or slot is empty then insert
        if (!arr[i] || value > arr[i]) {
            arr.unshift(value);
            break;
        }
    }
    // truncate the array back to the original size
    return arr.slice(0, size)
}

console.log(heap_k_largest(array1, 4));