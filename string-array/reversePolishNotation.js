// https://www.programcreek.com/2012/12/leetcode-evaluate-reverse-polish-notation/
const expression = ['2', '1', '+', '3', '*']
const expression2 = ["4", "13", "5", "/", "+"]

function parseExpression(exp) {
    const ops = ['+', '-', '*', '/']
    const values = []
    
    exp.forEach(el => {
        console.log(values)
        //check to see if its a operator
        if (ops.includes(el)) {
            //take the last 2 values and operate on it
            values.push(operateOn(values[values.length - 2], values[values.length - 1], el))
            //remove the 2 that have been used
            values.splice(0,2)
        } else {
            //add the value to the stack
            values.push(el)
        }
    });

    return values[values.length - 1];


    function operateOn(value1, value2, operator) {
        let val1 = parseInt(value1)
        let val2 = parseInt(value2)
        switch (operator) {
            case '+':
                return val1 + val2
                break;
            case '-':
                return val1 - val2
                break;
            case '*':
                return val1 * val2
                break;
            case '/':
                return val1 / val2
                break;
            default:
                return 0;
                break;
        }
    }
}

console.log(parseExpression(expression))

console.log(parseExpression(expression2))