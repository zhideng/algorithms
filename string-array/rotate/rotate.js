// https://www.programcreek.com/2015/03/rotate-array-in-java/
// https://www.programcreek.com/2014/05/leetcode-reverse-words-in-a-string-ii-java/

const numbers = [1,2,3,4,5,6,7]
const sentence = 'the quick brown fox jumped over the jumped over the lazy dog'

function reverseArr(arr) {
    const reverse = []
    for(let i = (arr.length - 1); i >= 0; i--) {
        reverse.push(arr[i])
    }
    return reverse
}

function rotateSteps(arr, steps) {
    const rotated = arr.slice(0)

    for(let i = 0; i < steps; i++) {
        rotated.push(rotated[0])
        rotated.splice(0,1)
    }
    return rotated
}

function rotateSteps2(arr, steps) {
    const front = arr.slice(0, steps)
    const back = arr.slice(steps, arr.length)

    return back.concat(front)
}

function reverseWords(phrase) {
    let words = phrase.split(' ')
    let rev = reverseArr(words)
    let result = rev.join(' ')
    return result
}

console.log('orginal array: ' + numbers)
const rev = reverseArr(numbers)
console.log('reversed array: ' + rev)

console.log('rotated array: ' + rotateSteps(numbers, 3))

console.log('rotated array faster: ' + rotateSteps2(numbers, 3))

console.log(sentence)

console.log(reverseWords(sentence))
