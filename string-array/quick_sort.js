// implement quick sort

function quickSort(arr, low, high) {
    if (low < high) {
        // partition by picking a pivot and moving all values smaller to the left and larger to the right
        let partition_index = partition(arr, low, high);
        // console.log(arr);

        // recursively soft the smaller partition
        quickSort(arr, low, partition_index - 1)
        // console.log(arr);
        // recursively sort the larger partition
        quickSort(arr, partition_index + 1, high)
        console.log(arr);
    }
}

function swap(arr, i, j) {
    let temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}

function partition(arr, low, high) {
    let pivot = arr[high];
    let i = low;
    for (let j = low; j < high; j++) {
        if (arr[j] < pivot) {
            swap(arr, i, j);
            i++;
        }
    }
    swap(arr, i, high);
    return i;
}

let arr = [10, 80, 30, 90, 40, 50, 70, 20, 100];
quickSort(arr, 0, 8);
// console.log(arr);