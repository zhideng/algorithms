// https://www.programcreek.com/2014/05/leetcode-isomorphic-strings-java/

function isIsomorphic(str1, str2) {
    //create a char mapping from str1 to str2
    mapping = {};
    for (let i = 0; i < str1.length; i++) {
        if(mapping[str1[i]]) {
            //if the mapping is found then make sure that the character from str2 is equal to the mapping
            if(mapping[str1[i]] != str2[i]) {
                return false
            }  
        } else {
            //add the mapping
            mapping[str1[i]] = str2[i]
        }
    }
    return true
}

console.log(isIsomorphic('egg', 'add'))

console.log(isIsomorphic('foo', 'bar'))

console.log(isIsomorphic('paper', 'title'))