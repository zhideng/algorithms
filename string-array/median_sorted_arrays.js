// https://www.geeksforgeeks.org/median-two-sorted-arrays-different-sizes-ologminn-m/
/*
    initial values
    min index = 0
    max index = length of smallest array
    n = 1st array length
    m = 2nd array length
    i = (min + max) / 2
    j = (n + m + 1) / 2 - i
*/

const array1 = [-5, 3, 6, 12, 15]
const array2 = [-12, -10, -6, -3, 4, 10]

function median_two_sorted_arrays(arr1, arr2) {
    // get the initial values
    let min_index = 0
    let max_index = arr1.length < arr2.length ? arr1.length : arr2.length
    let i = 0
    let j = 0

    while (min_index <= max_index) {
        i = Math.floor((min_index + max_index) / 2)
        j = Math.floor((arr1.length + arr2.length + 1) / 2) - i
        //compare haves
        let a1 = arr1[i - 1]
        let b1 = arr2[j]
        let a2 = arr1[i]
        let b2 = arr2[j - 1]
        // if ((arr1[i - 1] < arr2[j]) && (arr2[j-1] < arr1[i])) {
        if ((arr1[i - 1] < arr2[j]) && (arr2[j - 1] < arr1[i] || arr1[i] == undefined )) {
            // we have reached our corect halves
            return arr1[i - 1] < arr2[j - 1] ? arr2[j - 1] : arr1[i - 1]
        } else {
            //recalculate min
            min_index = i + 1
        }
    }

}

// console.log(median_two_sorted_arrays(array1, array2))

const array3 = [2, 3, 5, 8]
const array4 = [10, 12, 14, 16, 18, 20]

console.log(median_two_sorted_arrays(array3, array4))
