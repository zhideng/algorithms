// https://www.programcreek.com/2012/12/leetcode-word-ladder/

//simple queue using array
function Queue() {

    let _queue = []

    return {
        enqueue: function (item) {
            _queue.push(item)
        },
        dequeue: function () {
            let item = _queue[0]
            _queue.splice(0, 1)
            return item
        },
        count: function () {
            return _queue.length
        }
    }
}

function WordNode(word, steps) {
    return {
        word: word,
        steps: steps
    }
}

function wordLadder(startWord, endWord, dict) {
    //add the end word to the dict
    dict.push(endWord)
    
    let queue = new Queue()

    queue.enqueue(new WordNode(startWord, 1))

    while (queue.count() > 0) {
        //get a word off the queue
        let currentItem = queue.dequeue()

        //if the word matches the end word then we stop
        if (currentItem.word == endWord) {
            return currentItem.steps
        }

        //go through each of the characters in the current word
        let wordArray = currentItem.word.split('')
        for (let i = 0; i < wordArray.length; i++) {
            //go through each of the alphabets and replace the current postion
            for (let j = 0; j < 26; j++) {
                //save the current char
                let orig_char = wordArray[i]
                wordArray[i] = (j+10).toString(36)
                let new_word = wordArray.join('')

                //check to see if the new word is in the dictionary
                if(dict.includes(new_word)) {
                    //if the word is matched add to the queue and remove from the dict
                    queue.enqueue(new WordNode(new_word, currentItem.steps + 1))
                    dict.splice(dict.indexOf(new_word),1)
                }
                //restore the orignal word array
                wordArray[i] = orig_char
            }
        }
    }

    return 0;
}

console.log(wordLadder('hit', 'cog', ['hot', 'dot', 'dog', 'lot', 'log']))

function WordNodeList(word, word_list) {
    return {
        word: word,
        words: word_list
    }
}

function wordLadderPaths(startWord, endWord, dict) {
    //add the end word to the dict
    dict.push(endWord)

    let queue = new Queue()

    //this is a list of paths that resulted in a successful find
    let paths = []

    queue.enqueue(new WordNodeList(startWord, [startWord]))

    while (queue.count() > 0) {
        //get a word off the queue
        let currentItem = queue.dequeue()

        //if the word matches the end word then we stop
        if (currentItem.word == endWord) {
            paths.push(currentItem)
        }

        //go through each of the characters in the current word
        let wordArray = currentItem.word.split('')
        for (let i = 0; i < wordArray.length; i++) {
            //go through each of the alphabets and replace the current postion
            for (let j = 0; j < 26; j++) {
                //save the current char
                let orig_char = wordArray[i]
                let new_char = (j + 10).toString(36)
                //check to make sure its not the same as the original
                if (orig_char != new_char) {
                    wordArray[i] = new_char
                    let new_word = wordArray.join('')

                    //check to see if the new word is in the dictionary, can only match once
                    if (dict.includes(new_word) && !currentItem.words.includes(new_word)) {
                        //if the word is matched add to the queue and remove from the dict
                        let word_list = currentItem.words.slice(0)
                        word_list.push(new_word)
                        queue.enqueue(new WordNodeList(new_word, word_list))
                        //dict.splice(dict.indexOf(new_word),1)
                    }
                    //restore the orignal word array
                    wordArray[i] = orig_char
                }
            }
        }
    }

    return paths;
}

console.log(wordLadderPaths('hit', 'cog', ['hot', 'dot', 'dog', 'lot', 'log']))