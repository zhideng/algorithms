// https://www.programcreek.com/2014/06/leetcode-wildcard-matching-java/

function isMatch(input, pattern) {
    // take the first character from the pattern and interate through the input
    // if a match occurs then move to the next value in the pattern
    // once the pattern does not match then we have to go back to the first character from the pattern
    let char = pattern[0]
    let input_index = 0
    let pattern_index = 0
    while (input_index < input.length) {
        char = pattern[pattern_index]
        // if the char is * or ? then automatically mark as match
        let isWildcard = (char == '?' || char == '*') ? true : false
        if (input[input_index] == char || isWildcard) {
            if (pattern_index == pattern.length - 1) {
                // we have found all the patterns
                return true
            } else {
                pattern_index++
                // char = pattern[pattern_index]
            }
        } else {
            if (pattern_index > 0) {
                //if there was a previous pattern match decrement the input index so we start over from that point
                input_index--
            }
            // once we find a mismatch then we reset the pattern_index
            pattern_index = 0
        }
        input_index++
    }
    return false
}

console.log(isMatch('cabcab', '*bc'))