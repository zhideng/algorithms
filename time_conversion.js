// https://www.hackerrank.com/challenges/time-conversion/problem

function convert(t) {
    // extract the AM/PM from the end
    let amPM = t.substring(t.length - 2, t.length);
    // split the time up and get 3 values
    let timeValues = t.substring(0, t.length - 2).split(":");

    if (amPM == "PM") {
        if(timeValues[0] != "12") {
            let military = Number.parseInt(timeValues[0]) + 12
            timeValues[0] = military;
        }
    } else {
        // just one edge case when it's AM and 12 we need to change to 00
        if(timeValues[0] == "12") {
            timeValues[0] = "00";
        }
    }

    return timeValues.join(":");
}

console.log(convert("07:59:00PM"));