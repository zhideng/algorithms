// https://www.interviewbit.com/problems/rotate-list/

var Node = function (value) {
    return {
        value: value,
        next: null
    }
}

function PrintList(L) {
    let node = L;
    while (node) {
        console.log(node.value);
        node = node.next;
    }
}

function rotate_list(L, k) {
    if (k <= 0 || !L) {
        return L;
    }
    let items = []
    let currentNode = L;
    let counter = 0;
    // since we don't know how many items are in the linked list we need to get to the end first
    while (currentNode) {
        items.push(currentNode);
        currentNode = currentNode.next;
        counter++;
    }
    let correctedK = k;
    if (k >= counter) {
        correctedK = k % counter;
    }

    if (correctedK > 0) {
        items[items.length - correctedK - 1].next = null;
        items[items.length - 1].next = items[0];
        return items[items.length - correctedK];
    } else {
        return L;
    }
    
}

let node = new Node(1);
node.next = new Node(2);
node.next.next = new Node(3);
node.next.next.next = new Node(4);
node.next.next.next.next = new Node(5);
node.next.next.next.next.next = new Node(6);

let single = new Node(1);

PrintList(rotate_list(node, 7));