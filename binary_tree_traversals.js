// https://www.geeksforgeeks.org/bfs-vs-dfs-binary-tree/

var Node = function(value) {
    return {
        data: value,
        left: null,
        right: null
    }
}

/*
     1
    / \
   2   3
  / \
 4   5
*/
let root = new Node(1);
root.left = new Node(2);
root.right = new Node(3);
root.left.left = new Node(4);
root.left.right = new Node(5);
// root.left.left.left = new Node(6);
// root.left.left.right = new Node(7);

// breadth first search (BFS) or level order traversal
// https://www.geeksforgeeks.org/level-order-tree-traversal/

function BFSUtil(node, order) {
    if(node == null) {
        return;
    }

    if(node.left) {
        order.push(node.left.data);
    }

    if(node.right) {
        order.push(node.right.data);
    }
    BFSUtil(node.left, order)
    BFSUtil(node.right, order);

    return order;
}

function BFS(node) {
    let order = [];
    order.push(node.data);
    order = BFSUtil(node, order);
    return order;
}

console.log(BFS(root));

// DFS traversals
// https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/

function PreOrderUtil(node, order) {
    if(node == null) {
        return order;
    }
    order.push(node.data);

    order= PreOrderUtil(node.left, order);
    order = PreOrderUtil(node.right, order);

    return order;
}

function PreOrder(node) {
    return PreOrderUtil(node, []);
}

console.log(PreOrder(root));

function InOderUtil(node, order) {

    if(node == null) {
        return order;
    }

    order = InOderUtil(node.left, order);
    order.push(node.data);
    order = InOderUtil(node.right, order);

    return order;

}

function InOrder(node) {
    return InOderUtil(node, []);
}

console.log(InOrder(root));

function PostOrderUtil(node, order) {
    if(node == null) {
        return order;
    }

    order = PostOrderUtil(node.left, order);
    order = PostOrderUtil(node.right, order);
    order.push(node.data);

    return order;
}

function PostOrder(node) {
    return PostOrderUtil(node, []);
}

console.log(PostOrder(root));