// https://www.interviewbit.com/problems/regular-expression-match/

function isMatch2(value, expr) {

    if (value.length == 0 || expr.length == 0) {
        return 0;
    }
    // we need to return if the expr (expression) matches the given value string
    let match = 0;
    //traverse the value from first to last and try and match every character in the expression
    let textArray = value.split("");
    let exprArray = expr.split("");
    let textIndex = -1; // init to -1 when we start
    let matchCount = 0; // right now not using singe text index is working
    let expressMatches = []; // the index in the expression array that has matched
    for (let i = 0; i < exprArray.length; i++) {

        for (j = textIndex + 1; j < textArray.length; j++) {
            textIndex = j;
            if (exprArray[i] == textArray[j]) {
                // this is a match and we need to break and move to the next character in the expression
                matchCount++;
                expressMatches.push(i)
                break;
            } else if (exprArray[i] == "?") {
                // this is wildcard for single character so treat this as a match
                matchCount++;
                expressMatches.push(i)
                break;
            } else if (exprArray[i] == "*") {
                // this is wildcard that matches any so treat as match but don't break
                matchCount++;
                // only add if not already in array
                if (expressMatches.indexOf(i) == -1) {
                    expressMatches.push(i);
                }
            }
        }
    }

    if (matchCount == textArray.length && expressMatches.length == exprArray.length) {
        match = 1;
    }

    return match;
}

function isMatch(value, expr) {

    if (value.length == 0 || expr.length == 0) {
        return 0;
    }
    // we need to return if the expr (expression) matches the given value string
    let match = 0;
    //traverse the value from first to last and try and match every character in the expression
    let textArray = value.split("");
    let exprArray = expr.split("");
    let textIndex = 0; // init to -1 when we start
    let matchCount = 0; // right now not using singe text index is working
    let expressMatches = []; // the index in the expression array that has matched
    for (let i = 0; i < exprArray.length; i++) {

        // handle situation where we are out of text values but there are still remaining expression values
        if(textIndex >= textArray.length) {
            if(exprArray[i] == "?" || exprArray[i] == "*") {
                expressMatches.push(i);
            }
        } else {
            while (textIndex < textArray.length) {
                textIndex++;
                if (textArray[textIndex - 1] == exprArray[i]) {
                    // this is a match and we need to break and move to the next character in the expression
                    matchCount++;
                    expressMatches.push(i)
                    break;
                } else if (exprArray[i] == "?") {
                    // this is wildcard for single character so treat this as a match
                    matchCount++;
                    expressMatches.push(i)
                    break;
                } else if (exprArray[i] == "*") {
                    // this is wildcard that matches any so treat as match but don't break
                    matchCount++;
                    // only add if not already in array
                    if (expressMatches.indexOf(i) == -1) {
                        expressMatches.push(i);
                    }
                }
            }
        }

    }

    if (matchCount == textArray.length && expressMatches.length == exprArray.length) {
        match = 1;
    }

    return match;
}

console.log(isMatch("aa", "a")); //0
console.log(isMatch("aa", "aa")); //1
console.log(isMatch("aaa", "aa")); //0
console.log(isMatch("aa", "*")); //1
console.log(isMatch("aa", "a*"));  //1
console.log(isMatch("ab", "?*")); //1
console.log(isMatch("aab", "c*a*b")); //0
console.log(isMatch("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "*b")); // 0
console.log(isMatch("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "a**************************************************************************************")); // 1
console.log(isMatch("bbbcbcb", "**b")) // 1