/*
Given an unsorted array of numbers, find the ‘K’ largest numbers in it.

Note: For a detailed discussion about different approaches to solve this problem, take a look at Kth Smallest Number.

Input: [3, 1, 5, 12, 2, 11], K = 3
Output: [5, 12, 11]

Input: [5, 12, 11, -1, 12], K = 3
Output: [12, 11, 12]
*/

const find_k_largest_numbers = function (nums, k) {
    result = []

    // go through each value in then array, and add to the array if the number is greater than any number in the array
    // if the array does not have k numbers then add

    for (let i = 0; i < nums.length; i++) {
        if (result.length < k) {
            result.push(nums[i]);
        } else {
            // just check the first one it should be sorted in ascending order, if the number is larger remove the first
            // one and add and sort
            if(nums[i] > result[0]) {
                result.push(nums[i]);
                result.shift();
            }
        }
        // so we are sorting each time mimicking a heap, javascript doesn't have a heap data structure
        // if we were to use sorting the easiest way would be sort the input array and take the last k numbers for O(nlogn)
        result.sort((a, b) => {
            return a - b;
        })
    }

    return result;
};


console.log(`Here are the top K numbers: ${find_k_largest_numbers([3, 1, 5, 12, 2, 11], 3)}`)
console.log(`Here are the top K numbers: ${find_k_largest_numbers([5, 12, 11, -1, 12], 3)}`)
