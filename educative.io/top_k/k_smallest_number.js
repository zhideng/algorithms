/*
Given an unsorted array of numbers, find Kth smallest number in it.

Please note that it is the Kth smallest number in the sorted order, not the Kth distinct element.

Note: For a detailed discussion about different approaches to solve this problem, take a look at Kth Smallest Number.

Input: [1, 5, 12, 2, 11, 5], K = 3
Output: 5
Explanation: The 3rd smallest number is '5', as the first two smaller numbers are [1, 2].

Input: [1, 5, 12, 2, 11, 5], K = 4
Output: 5
Explanation: The 4th smallest number is '5', as the first three small numbers are [1, 2, 5].

Input: [5, 12, 11, -1, 12], K = 3
Output: 11
Explanation: The 3rd smallest number is '11', as the first two small numbers are [5, -1].
*/

// this problem is the same as top_k_numbers but we are doing smallest instead so using minheap

const find_Kth_smallest_number = function(nums, k) {
    result = []

    // go through each value in then array, and add to the array if the number is greater than any number in the array
    // if the array does not have k numbers then add

    for (let i = 0; i < nums.length; i++) {
        if (result.length < k) {
            result.push(nums[i]);
        } else {
            // just check the first one it should be sorted in ascending order, if the number is larger remove the first
            // one and add and sort
            if(nums[i] < result[k-1]) {
                result.unshift(nums[i]);
                result.pop();
            }
        }
        // so we are sorting each time mimicking a heap, javascript doesn't have a heap data structure
        // if we were to use sorting the easiest way would be sort the input array and take the last k numbers for O(nlogn)
        result.sort((a, b) => {
            return a - b;
        })
    }

    return result[k-1];
  };
  
  
  console.log(`Kth smallest number is: ${find_Kth_smallest_number([1, 5, 12, 2, 11, 5], 3)}`)
  // since there are two 5s in the input array, our 3rd and 4th smallest numbers should be a '5'
  console.log(`Kth smallest number is: ${find_Kth_smallest_number([1, 5, 12, 2, 11, 5], 4)}`)
  console.log(`Kth smallest number is: ${find_Kth_smallest_number([5, 12, 11, -1, 12], 3)}`)
  