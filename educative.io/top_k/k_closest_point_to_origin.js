/*
Given an array of points in the a 2D2D plane, find ‘K’ closest points to the origin.

Input: points = [[1,2],[1,3]], K = 1
Output: [[1,2]]
Explanation: The Euclidean distance between (1, 2) and the origin is sqrt(5).
The Euclidean distance between (1, 3) and the origin is sqrt(10).
Since sqrt(5) < sqrt(10), therefore (1, 2) is closer to the origin.

Input: point = [[1, 3], [3, 4], [2, -1]], K = 2
Output: [[1, 3], [2, -1]]
*/

// we can do this with the same exact algo as the k_smallest_number just saving the points and
// compairing using distance formula sqrt(x^2 + y^2) = distance from origin

class Point {

    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    get_point() {
        return "[" + this.x + ", " + this.y + "] ";
    }
};

// this function returns the distance of point from origin (0,0)
function distance(point) {
    return Math.sqrt(Math.pow(point.x, 2) + Math.pow(point.y, 2));
}

const find_closest_points = function (points, k) {
    result = []

    // go through each value in then array, and add to the array if the number is greater than any number in the array
    // if the array does not have k numbers then add

    for (let i = 0; i < points.length; i++) {
        if (result.length < k) {
            result.push(points[i]);
        } else {
            // just check the first one it should be sorted in ascending order, if the number is larger remove the first
            // one and add and sort
            if (distance(points[i]) < distance(result[k - 1])) {
                result.unshift(points[i]);
                result.pop();
            }
        }
        // so we are sorting each time mimicking a heap, javascript doesn't have a heap data structure
        // if we were to use sorting the easiest way would be sort the input array and take the last k numbers for O(nlogn)
        result.sort((a, b) => {
            return distance(a) - distance(b);
        })
    }

    return result;
};


points = find_closest_points([new Point(1, 3), new Point(3, 4), new Point(2, -1)], 2)
result = "Here are the k points closest the origin: ";
for (i = 0; i < points.length; i++)
    result += points[i].get_point();
console.log(result);
