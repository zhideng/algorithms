/*
Given the weights and profits of ‘N’ items, we are asked to put these items in a knapsack which has a capacity ‘C’. The goal is to get the maximum profit out of the items in the knapsack. Each item can only be selected once, as we don’t have multiple quantities of any item.

Let’s take the example of Merry, who wants to carry some fruits in the knapsack to get maximum profit. Here are the weights and profits of the fruits:

Items: { Apple, Orange, Banana, Melon }
Weights: { 2, 3, 1, 4 }
Profits: { 4, 5, 3, 7 }
Knapsack capacity: 5

Let’s try to put various combinations of fruits in the knapsack, such that their total weight is not more than 5:

Apple + Orange (total weight 5) => 9 profit
Apple + Banana (total weight 3) => 7 profit
Orange + Banana (total weight 4) => 8 profit
Banana + Melon (total weight 5) => 10 profit

This shows that Banana + Melon is the best combination as it gives us the maximum profit and the total weight does not exceed the capacity.

Problem Statement #
Given two integer arrays to represent weights and profits of ‘N’ items, we need to find a subset of these items which will give us maximum profit such that their cumulative weight is not more than a given number ‘C’. Each item can only be selected once, which means either we put an item in the knapsack or we skip it.

Basic Solution #
A basic brute-force solution could be to try all combinations of the given items (as we did above), allowing us to choose the one with maximum profit and a weight that doesn’t exceed ‘C’. Take the example of four items (A, B, C, and D), as shown in the diagram below. To try all the combinations, our algorithm will look like:
*/

// the answer uses recursion to solve this by getting all possible combos. I will use a loop instead but equally bad Big O

let solveKnapsack = function (profits, weights, capacity) {
    function knapsackRecursive(profits, weights, capacity, currentIndex) {
        // base checks
        if (capacity <= 0 || currentIndex >= profits.length) return 0;

        // recursive call after choosing the element at the currentIndex
        // if the weight of the element at currentIndex exceeds the capacity, we shouldn't process this
        let profit1 = 0;
        if (weights[currentIndex] <= capacity) {
            profit1 =
                profits[currentIndex] +
                knapsackRecursive(profits, weights, capacity - weights[currentIndex], currentIndex + 1);
        }

        // recursive call after excluding the element at the currentIndex
        const profit2 = knapsackRecursive(profits, weights, capacity, currentIndex + 1);

        return Math.max(profit1, profit2);
    }

    return knapsackRecursive(profits, weights, capacity, 0);
};

// my brute force using loops instead of recursion, nest 2 loops i and j and keep adding until we go past capacity
// and then move j, once j reaches the end then we move i Big O of this is probably n^2 which is bad
function solveKnapsackLoops(profits, weights, capacity) {

    let currentWeight = 0;
    let maxProfit = 0;
    for (let i = 0; i < weights.length; i++) {
        let startingWeight = weights[i];
        let startingProfit = profits[i];
        for (let j = i + 1; j < weights.length; j++) {
            let currentWeight = startingWeight;
            let currentProfit = startingProfit;
            let addIndex = 0;
            // try and keep adding to the knapsack until its overweight then shift j
            while (currentWeight < capacity) {
                if (j + addIndex < weights.length && currentWeight + weights[j + addIndex] <= capacity) {
                    // add the item to the knapsack and update profit
                    currentWeight += weights[j + addIndex];
                    currentProfit += profits[j + addIndex];
                    addIndex++;
                } else {
                    // this means we cannot add addition and we must move the j counter to the next place
                    break;
                }
            }
            maxProfit = Math.max(maxProfit, currentProfit);
        }
    }

    return maxProfit;
}

// this is the bottom up dynamic programming solution has complexity of O(N * C) where N is total number of items and C is capacity
let solveKnapsack_dp = function (profits, weights, capacity) {
    const n = profits.length;
    if (capacity <= 0 || n == 0 || weights.length != n) return 0;

    const dp = Array(profits.length)
        .fill(0)
        .map(() => Array(capacity + 1).fill(0));

    // populate the capacity=0 columns; with '0' capacity we have '0' profit
    for (let i = 0; i < n; i++) dp[i][0] = 0;

    // if we have only one weight, we will take it if it is not more than the capacity
    for (let c = 0; c <= capacity; c++) {
        if (weights[0] <= c) dp[0][c] = profits[0];
    }

    // process all sub-arrays for all the capacities
    for (let i = 1; i < n; i++) {
        for (let c = 1; c <= capacity; c++) {
            let profit1 = 0,
                profit2 = 0;
            // include the item, if it is not more than the capacity
            if (weights[i] <= c) profit1 = profits[i] + dp[i - 1][c - weights[i]];
            // exclude the item
            profit2 = dp[i - 1][c];
            // take maximum
            dp[i][c] = Math.max(profit1, profit2);
        }
    }

    // maximum profit will be at the bottom-right corner.
    return dp[n - 1][capacity];
};

var profits = [1, 6, 10, 16];
var weights = [1, 2, 3, 5];
console.log(`Total knapsack profit: ---> ${solveKnapsack(profits, weights, 7)}`);
console.log(`Total knapsack profit: ---> ${solveKnapsack(profits, weights, 6)}`);

console.log(`Total knapsack profit: ---> ${solveKnapsackLoops(profits, weights, 7)}`);
console.log(`Total knapsack profit: ---> ${solveKnapsackLoops(profits, weights, 6)}`);

console.log(`Total knapsack profit: ---> ${solveKnapsack_dp(profits, weights, 7)}`);
console.log(`Total knapsack profit: ---> ${solveKnapsack_dp(profits, weights, 6)}`);