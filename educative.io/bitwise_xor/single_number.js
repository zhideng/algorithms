/*
In a non-empty array of integers, every number appears twice except for one, find that single number.

Input: 1, 4, 2, 1, 3, 2, 3
Output: 4

Input: 7, 9, 7
Output: 9


*/


function find_single_number(arr) {
    let xor = arr[0];
    for(let i = 1; i < arr.length; i++) {
        xor = xor ^ arr[i];
    }

    return xor;
  }
    
  console.log(find_single_number([1, 4, 2, 1, 3, 2, 3]));
