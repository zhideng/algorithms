/*
Given an array of characters where each character represents a fruit tree, you are given two baskets and your goal is to put maximum number of fruits in each basket. The only restriction is that each basket can have only one type of fruit.

You can start with any tree, but once you have started you can’t skip a tree. You will pick one fruit from each tree until you cannot, i.e., you will stop when you have to pick from a third fruit type.

Write a function to return the maximum number of fruits in both the baskets.
*/

function fruits_into_baskets(trees) {
    let types = [];
    let startIndex = 0;
    let endIndex = 0;
    let counter = 0;
    let maxCounter = 0;

    while(endIndex < trees.length) {
        if(types.length < 2) {
            types.push(trees[endIndex]);
            endIndex++;
            counter++;
            maxCounter = Math.max(maxCounter, counter);
        } else {
            // now both baskets have fruit, check to see if we have one of the fruits
            if(types.includes(trees[endIndex])) {
                endIndex++;
                counter++;
                maxCounter = Math.max(maxCounter, counter);
            } else {
                // we have a new fruit and have to remove all items from the start that are the same
                maxCounter = Math.max(maxCounter, counter);
                let fruitType = types[0];
                startIndex++;
                counter--;
                while(fruitType == trees[startIndex]) {
                    startIndex++;
                    counter--;
                }
                // remove the fruit type
                types.shift();
                types.push(trees[endIndex]);
                endIndex++;
                counter++;
            }
        }
    }

    return maxCounter;

}

console.log(`Maximum number of fruits: ${fruits_into_baskets(['A', 'B', 'C', 'A', 'C'])}`);
console.log(`Maximum number of fruits: ${fruits_into_baskets(['A', 'B', 'C', 'B', 'B', 'C'])}`);