/*
Given a string, find the length of the longest substring which has no repeating characters.

Input: String="aabccbb"
Output: 3
Explanation: The longest substring without any repeating characters is "abc".

Input: String="abbbb"
Output: 2
Explanation: The longest substring without any repeating characters is "ab".

Input: String="abccde"
Output: 3
Explanation: Longest substrings without any repeating characters are "abc" & "cde".
*/
// TODO not exactly right, need to add hashmap lookup and shift the string to the right index position
// the sample strings given have repeating values next to each other it could be abacdd so the longest is bacd
const non_repeat_substring = function (str) {

    let maxCount = 0;
    let currentCount =0;
    let prevChar = '';

    for(let i = 0; i < str.length; i++) {
        if(str.charAt(i) != prevChar) {
            // not the same value increment count
            currentCount++;
            // check to see if it is the largest count
            maxCount = Math.max(maxCount, currentCount);
        } else {
            // they are the same so we need to reset
            currentCount = 1;
        }
        prevChar = str.charAt(i);
    }

    return maxCount;
};

console.log(`Length of the longest substring: ${non_repeat_substring('aabccbb')}`);
console.log(`Length of the longest substring: ${non_repeat_substring('abbbb')}`);
console.log(`Length of the longest substring: ${non_repeat_substring('abccde')}`);