let A = [2, 1, 5, 2, 3, 2]
let B = [2, 1, 5, 2, 8]
let C = [2, 1, 5, 2, 3, 2]

function smallest_subarray_sum(A, sum) {

    let end_index = 0;
    let current_sum =0;
    let start_index = 0;
    let min_length = Infinity;

    // move through the array until the end index reaches the end
    for(end_index = 0; end_index < A.length; end_index++) {
        // add to the current sum
        current_sum += A[end_index];
        //once the current sum crosses the sum threshold we should try moving the start index
        while(current_sum >= sum) {
            min_length = Math.min(min_length, (end_index - start_index + 1))
            //remove from the front of the index
            current_sum -= A[start_index];
            start_index++;
        }
    }

    return min_length;
}

console.log(smallest_subarray_sum(C, 8));