let A = [2, 3, 4, 1, 5]

function max_sum(A, k) {
    if (A.length == 0 || k == 0) {
        return 0;
    }

    if (k > A.length) {
        return A.reduce((a, b) => {
            return a + b;
        })
    }

    let start_index = 0;
    let end_index = 0;
    let max_start_index = 0;
    let max_end_index = 0;
    let max_sum = 0;
    let count = 0;
    let sum = 0

    for (let i = 0; i < A.length || end_index == A.length - 1; i++) {
        // build up the sum until the kth value
        if (count < k) {
            count++;
            sum += A[i];
            end_index = i;
        } else {
            // subtract from sum
            sum -= A[start_index];
             // move the start index
             start_index += 1;
             // add the next value
             end_index += 1;
             sum += A[end_index];
        }

        //check to see if its the larges sum
        if (sum > max_sum) {
            max_sum = sum;
            max_start_index = start_index;
            max_end_index = end_index;
        }
    }

    return max_sum;
}

console.log(max_sum(A, 2));