function longest_substring(str, k) {
    let values = []; // this is array of distinct chars
    let start_index = 0;
    let end_index = 0;
    let max_start_index = 0;
    let max_end_index = 0;
    let max_length = 0;

    for (let i = 0; i < str.length; i++) {
        if (values.length < k && !values.includes(str[i])) {
            // keep adding to the distinct array until filled
            values.push(str[i])
        } else if(values.length == k && !values.includes(str[i])) {
            // we need to shift to the next start index when we encounter a new character that pushes us pass k distinct
            start_index = find_start_index(str, i, k);
            //remove the value from the values
            values.shift();
            values.push(str[i]);
        }
        end_index = i;

        // check to see if the new start and end index is longer than previous
        if (end_index - start_index + 1 > max_length) {
            max_length = end_index - start_index + 1
        }
    }

    return max_length;
}

function find_start_index(str, end, k) {
    let values = [];
    let index = end;
    // go from the end to the front until we have found k unique values
    for (let i = end; i >= 0; i--) {
        if (values.length < k && !values.includes(str[i])) {
            values.push(str[i]);
            index = i;
        } else {
            if(values.includes(str[i])) {
                index = i;
            } else {
                break;  // the next unique character goes beyond the limit so we can quit
            }
            
        }
    }
    return index;
}

console.log(longest_substring('araaci', 2));
console.log(longest_substring('araaci', 1));
console.log(longest_substring('cbbebi', 3));