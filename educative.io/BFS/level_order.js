/*
Given a binary tree, populate an array to represent its level-by-level traversal. You should populate the values of all nodes of each level from left to right in separate sub-arrays.
      [1]
      / \
    [2]  [3]
    / \  / \
  [4][5][6][7]

output 1,2,3,4,5,6,7
*/

class TreeNode {

    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }
};

// recursive route could be as bad as O(n^2)
function traverse_util(node, output) {
    if(node == null)
        return;

    //traverse the left and then the right
    if(node.left) {
        output.push(node.left.value);
    }
    if(node.right) {
        output.push(node.right.value);
    }

    //recursive traverse the child nodes
    traverse_util(node.left, output);
    traverse_util(node.right, output);

    return output;
}

const traverse = function (root) {
    result = [];
    
    result.push(root.value);
    
    result = traverse_util(root, result);

    return result;
};

// using queue is faster it should be O(n) since we are visiting each node once
function traverse_queue(node) {
    let result = [];
    let queue = [];     // use array as a queue mechanism

    queue.push(root);   // start with first node

    while(queue.length > 0) {
       let currentNode = queue.shift();
       result.push(currentNode.value);

       //add all the children to the queue
       if(currentNode.left) {
           queue.push(currentNode.left);
       }
       if(currentNode.right) {
           queue.push(currentNode.right);
       }
    }

    return result;
}


var root = new TreeNode(12);
root.left = new TreeNode(7);
root.right = new TreeNode(1);
root.left.left = new TreeNode(9);
root.right.left = new TreeNode(10);
root.right.right = new TreeNode(5);
console.log(`Level order traversal: ${traverse_queue(root)}`);