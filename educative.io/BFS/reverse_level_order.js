/*
Given a binary tree, populate an array to represent its level-by-level traversal in reverse order, i.e., the lowest level comes first. You should populate the values of all nodes in each level from left to right in separate sub-arrays.
      [1]
      / \
    [2]  [3]
    / \  / \
  [4][5][6][7]

output 4,5,6,7,2,3,1

*/

class TreeNode {

    constructor(value) {
      this.value = value;
      this.left = null;
      this.right = null; 
    }
  };
  
  const traverse = function(root) {
    let result = [];
    let queue = [];     // use array as a queue mechanism

    queue.push(root);   // start with first node

    while (queue.length > 0) {
        let levelSize = queue.length;   // this determines how wide is the level
        let levelOutput = [];

        for (let i = 0; i < levelSize; i++) {
            //go through each item in the queue for the size and add to levelOutput
            let currentNode = queue.shift();
            levelOutput.push(currentNode.value);

            //add all the children to the queue
            if (currentNode.left) {
                queue.push(currentNode.left);
            }
            if (currentNode.right) {
                queue.push(currentNode.right);
            }
        }

        result.unshift(levelOutput);   // add the level to the result array
    }

    return result;
  }
  
  var root = new TreeNode(12)
  root.left = new TreeNode(7)
  root.right = new TreeNode(1)
  root.left.left = new TreeNode(9)
  root.right.left = new TreeNode(10)
  root.right.right = new TreeNode(5)
  console.log(`Reverse level order traversal: ${traverse(root)}`)