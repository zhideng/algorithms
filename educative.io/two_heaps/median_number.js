/*
Design a class to calculate the median of a number stream. The class should have the following two methods:

insertNum(int num): stores the number in the class
findMedian(): returns the median of all numbers inserted in the class
If the count of numbers inserted in the class is even, the median will be the average of the middle two numbers.

1. insertNum(3)
2. insertNum(1)
3. findMedian() -> output: 2
4. insertNum(5)
5. findMedian() -> output: 3
6. insertNum(4)
7. findMedian() -> output: 3.5

*/

// class MedianOfAStream {
//     constructor() {
//         this.maxHeap = [];
//         this.minHeap = [];
//     }
//     insert_num(num) {
//      // TODO: Write your code here
//      return -1;
//     }

//     find_median(self) {
//       // TODO: Write your code here
//       return 0.0
//     }
//   };

function MedianOfAStream() {
    this.maxHeap = [];
    this.minHeap =[];

    this.insert_num = function(num) {
        // both of them are empty put into max
        if(this.maxHeap.length == 0) {
            this.maxHeap.push(num);
        } else {
            // this scenario its either equal or has 1 more in maxHeap
            if(num <= this.maxHeap[this.maxHeap.length - 1]) {
                // this number is smaller than the maxHeap one insert and we wil rebalance
                this.maxHeap.push(num);
            } else {
                this.minHeap.push(num);
            }
        }
        //sort the heaps
        this.maxHeap.sort((a,b) => {
            return a - b;
        });
        this.minHeap.sort((a,b) => {
            return a - b;
        });
        // balance the heaps, the maxHeap should be equal or have 1 more than the minHeap
        if(this.maxHeap.length > this.minHeap.length) {
            while(this.maxHeap.length - this.minHeap.length > 1) {
                this.minHeap.push(this.maxHeap.pop());
            }
        }
        if(this.minHeap.length > this.maxHeap.length) {
            while(this.minHeap.length - this.maxHeap.length > 0) {
                this.maxHeap.push(this.minHeap.shift());
            }
        }
       
    }

    this.find_median = function() {
        if(this.maxHeap.length == this.minHeap.length) {
            return (this.maxHeap[this.maxHeap.length - 1] + this.minHeap[0]) / 2;
        } else {
            return (this.maxHeap[this.maxHeap.length - 1]);
        }
    }
}


var medianOfAStream = new MedianOfAStream()
medianOfAStream.insert_num(3)
medianOfAStream.insert_num(1)
console.log(`The median is: ${medianOfAStream.find_median()}`)
medianOfAStream.insert_num(5)
console.log(`The median is: ${medianOfAStream.find_median()}`)
medianOfAStream.insert_num(4)
console.log(`The median is: ${medianOfAStream.find_median()}`)