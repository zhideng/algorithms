/*
Given a set of numbers that might contain duplicates, find all of its distinct subsets.

Input: [1, 3, 3]
Output: [], [1], [3], [1,3], [3,3], [1,3,3]

Input: [1, 5, 3, 3]
Output: [], [1], [5], [3], [1,5], [1,3], [5,3], [1,5,3], [3,3], [1,3,3], [3,3,5], [1,5,3,3] 
*/

// this works but there is probably another way to get the subsets without lookup
const find_subsets = function (nums) {
    const subsets = [];
    const map = {}; // use this to track the combos that have been already covered
    // start by adding the empty subset
    subsets.push([]);
    for (i = 0; i < nums.length; i++) {
        currentNumber = nums[i];
        // we will take all existing subsets and insert the current number in them to create new subsets
        const n = subsets.length;
        for (j = 0; j < n; j++) {
            // create a new subset from the existing subset and insert the current element to it
            const set = subsets[j].slice(0); // clone the permutation
            set.push(currentNumber);
            let key = set.join("-");
            if (!(key in map)) {
                map[key] = "";
                subsets.push(set);
            }
        }
    }

    return subsets;
};

function find_subsets_no_map(nums) {
    const subsets = [];
    // start by adding the empty subset
    subsets.push([]);
    // sort the numbers
    nums.sort();
    let startIndex = 0;
    let endIndex = 0;
    for (i = 0; i < nums.length; i++) {
        currentNumber = nums[i];

        startIndex = 0; //reset the start Index on each iteration
        // if the current number is the same as the previous number then change the startIndex to the previous endIndex + 1
        if(i > 0 && currentNumber == nums[i-1]) {
            startIndex = endIndex;
        }

        // we will take all existing subsets and insert the current number in them to create new subsets
        endIndex = subsets.length;
        for (j = startIndex; j < endIndex; j++) {
            // create a new subset from the existing subset and insert the current element to it
            const set = subsets[j].slice(0); // clone the permutation
            set.push(currentNumber);
            subsets.push(set);
        }
    }

    return subsets;
}


console.log(`Here is the list of subsets: ${find_subsets([1, 3, 3])}`)
console.log(`Here is the list of subsets: ${find_subsets_no_map([1, 5, 3, 3])}`)