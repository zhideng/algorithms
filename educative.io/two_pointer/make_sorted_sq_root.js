/*
Given a sorted array, create a new array containing squares of all the number of the input array in the sorted order.

Input: [-2, -1, 0, 2, 3]
Output: [0, 1, 4, 4, 9]

Input: [-3, -1, 0, 1, 2]
Output: [0 1 1 4 9]
*/

let A = [-2, -1, 0, 2, 3];
let B = [-3, -1, 0, 1, 2];

function make_sorted_squares(A) {
    // we're going to start at both ends of the array and move towards the middle
    // whichever is larger we are going to add to the output array going from highest to lowest
    let output = new Array(A.length);
    output.fill(0); //create an output array that is the same size as the original and fill with zeros
    let highIndex = A.length - 1;
    let left = 0;
    let right = A.length - 1;

    while(left <= right) {
        let leftSquare = A[left] * A[left];
        let rightSquare = A[right] * A[right];

        // if the left is larger put that in the output and move the output index
        if(leftSquare > rightSquare) {
            output[highIndex] = leftSquare;
            left++;
        } else {
            // the right is either larger or equal at this point
            output[highIndex] = rightSquare;
            right--;
        }
        highIndex--;
    }

    return output;
}

console.log(make_sorted_squares(A));
console.log(make_sorted_squares(B));