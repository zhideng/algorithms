/*
Given an array of sorted numbers, remove all duplicates from it. You should not use any extra space; after removing the duplicates in-place return the new length of the array.

Input: [2, 3, 3, 3, 6, 9, 9]
Output: 4
Explanation: The first four elements after removing the duplicates will be [2, 3, 6, 9].

Input: [2, 2, 2, 11]
Output: 2
Explanation: The first two elements after removing the duplicates will be [2, 11].
*/

let A = [2, 3, 3, 3, 6, 9, 9];
let B = [2, 2, 2, 11];

function remove_duplicates(A) {
    let prev_value = -1
    let current_index = 0;

    while(current_index <= A.length - 1) {
        if(A[current_index] == prev_value) {
            // duplicate remove from array
            A.splice(current_index,1);
        } else {
            // different number, assign new prev_value
            prev_value = A[current_index];
            current_index++;
        }
    }

    return A;

}

function remove_duplicates_answer(arr) {
    // index of the next non-duplicate element
    let nextNonDuplicate = 1;
  
    let i = 1;
    while (i < arr.length) {
      if (arr[nextNonDuplicate - 1] !== arr[i]) {
        arr[nextNonDuplicate] = arr[i];
        nextNonDuplicate += 1;
      }
      i += 1;
    }
  
    return nextNonDuplicate;
  }

// console.log(remove_duplicates(A));
// console.log(remove_duplicates(B));
console.log(remove_duplicates_answer(A));