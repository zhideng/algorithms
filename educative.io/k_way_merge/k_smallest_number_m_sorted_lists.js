/*
Given ‘M’ sorted arrays, find the K’th smallest number among all the arrays.

Input: L1=[2, 6, 8], L2=[3, 6, 7], L3=[1, 3, 4], K=5
Output: 4
Explanation: The 5th smallest number among all the arrays is 4, this can be verified from the merged 
list of all the arrays: [1, 2, 3, 3, 4, 6, 6, 7, 8]

Input: L1=[5, 8, 9], L2=[1, 7], K=3
Output: 7
Explanation: The 3rd smallest number among all the arrays is 7.

*/

// this is the same as k_way_merge but I am not going to use the heap library.
// going to use an array instead to mimic the minheap
// this should take O(k log M) where M is the number of lists and k is kth smallest
const find_Kth_smallest = function (lists, k) {
    number = -1;
    let minHeap = [];

    for (let i = 0; i < lists.length; i++) {
        minHeap.push(lists[i]);
    }
    // sort the minHeap by the first item in the respective array
    minHeap.sort(sort_function);

    let counter = 0;

    while (counter < k) {
        // keep removing items until counter
        minHeap[0].shift(); // remove the lowest item
        // need to check if the array is empty, if it is then remove it entirely from minHeap
        if (minHeap[0].length == 0) {
            minHeap.shift();
        }
        counter++;
        // sort again
        minHeap.sort(sort_function);
    }

    return minHeap[0][0];
};

function sort_function(a, b) {
    if (a == null) {
        return b
    } else if (b == null) {
        return a
    } else {
        return a[0] - b[0];
    };
}


console.log(`Kth smallest number is: ${find_Kth_smallest([[2, 6, 8], [3, 6, 7], [1, 3, 4]], 5)}`)