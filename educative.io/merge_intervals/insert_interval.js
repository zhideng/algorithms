/*
Given a list of non-overlapping intervals sorted by their start time, insert a given interval at the correct position and merge all necessary intervals to produce a list that has only mutually exclusive intervals.


Input: Intervals=[[1,3], [5,7], [8,12]], New Interval=[4,6]
Output: [[1,3], [4,7], [8,12]]
Explanation: After insertion, since [4,6] overlaps with [5,7], we merged them into one [4,7].

Input: Intervals=[[1,3], [5,7], [8,12]], New Interval=[4,10]
Output: [[1,3], [4,12]]
Explanation: After insertion, since [4,10] overlaps with [5,7] & [8,12], we merged them into [4,12].

Input: Intervals=[[2,3],[5,7]], New Interval=[1,4]
Output: [[1,4], [5,7]]
Explanation: After insertion, since [1,4] overlaps with [2,3], we merged them into one [1,4].
*/

class Interval {
    constructor(start, end) {
        this.start = start;
        this.end = end;
    }

    print_interval() {
        process.stdout.write(`[${this.start}, ${this.end}]`);
    }
}

const insert = function (intervals, new_interval) {
    let merged = [];
    // we will go through each of the intervals and determine if the new one overlaps
    // if it does then we will take the min of the start and the max of the end
    let merged_interval = new_interval

    for (let i = 0; i < intervals.length; i++) {

        if(merged_interval.start > intervals[i].end ) {
            // new interval is greater than these intervals
            merged.push(intervals[i]);
        } else if ((intervals[i].start >= merged_interval.start && intervals[i].start <= merged_interval.end) ||
            intervals[i].end >= merged_interval.start && intervals[i].end <= merged_interval.end) {
            // the new interval overlaps
            merged_interval = new Interval(Math.min(intervals[i].start, new_interval.start), Math.max(intervals[i].end, new_interval.end));
            // edge case if we merge until the last item then we need to add
            if(i == intervals.length -1) {
                merged.push(merged_interval);
            }
        } else {
            merged.push(merged_interval);   //when we hit this point it means we are no longer overlapping and just need to add the rest
            merged.push(intervals[i]);
        }
    }

    return merged;
};

process.stdout.write('Intervals after inserting the new interval: ');
let result = insert([
    new Interval(1, 3),
    new Interval(5, 7),
    new Interval(8, 12),
], new Interval(4, 6));
for (i = 0; i < result.length; i++) {
    result[i].print_interval();
}
console.log();

process.stdout.write('Intervals after inserting the new interval: ');
result = insert([
    new Interval(1, 3),
    new Interval(5, 7),
    new Interval(8, 12),
], new Interval(4, 10));
for (i = 0; i < result.length; i++) {
    result[i].print_interval();
}
console.log();

process.stdout.write('Intervals after inserting the new interval: ');
result = insert([new Interval(2, 3),
new Interval(5, 7),
], new Interval(1, 4));
for (i = 0; i < result.length; i++) {
    result[i].print_interval();
}
console.log();