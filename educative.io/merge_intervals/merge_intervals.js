/*
 Given a list of intervals, merge all the overlapping intervals to produce a list that has only mutually exclusive intervals.

Intervals: [[1,4], [2,5], [7,9]]
Output: [[1,5], [7,9]]
Explanation: Since the first two intervals [1,4] and [2,5] overlap, we merged them into 
one [1,5].

Intervals: [[6,7], [2,4], [5,9]]
Output: [[2,4], [5,9]]
Explanation: Since the intervals [6,7] and [5,9] overlap, we merged them into one [5,9].

Intervals: [[1,4], [2,6], [3,5]]
Output: [[1,6]]
Explanation: Since all the given intervals overlap, we merged them into one.
*/

class Interval {
    constructor(start, end) {
        this.start = start;
        this.end = end;
    }

    get_interval() {
        return "[" + this.start + ", " + this.end + "]";
    }
}

// my answer interates through array and compares and is logn^2 which is not good
const merge = function (intervals) {
    merged = []
    let index = 0

    // we are going to go through each of the intervals and collapse them if needed until we reach the end of the array
    while (index != intervals.length - 1) {
        for (let i = index + 1; i < intervals.length; i++) {
            let mergeInterval = overlap(intervals[i], intervals[index])
            if (mergeInterval != null) {
                // this means we had to merge something
                intervals[i] = mergeInterval;
                // remove the index item
                intervals.shift();
                //reset the index back to zero
                index = -1; // need to offset to -1 because the incrementer happens on every while loop
                break;
            }
        }
        index++;
    }

    return intervals;
};

// determines if the intervals overlap each other
function overlap(intv1, intv2) {

    // find which find of merge we need
    if (intv1.start >= intv2.start && intv1.end <= intv2.end) {
        // 1 is inside 2
        return intv2;
    } else if (intv2.start >= intv1.start && intv2.end <= intv1.end) {
        // 2 is insside 1
        return intv1
    } else if (intv1.start > intv2.start && intv1.start < intv2.end) {
        // 1 overlaps 2 but 2 in front
        return new Interval(intv2.start, intv1.end)
    } else if (intv2.start > intv1.start && intv2.start < intv1.end) {
        // 2 overlaps 1 but 1 in front
        return new Interval(intv1.start, intv2.end)
    }

    return null
}

// answer from internet, this does nlogn because the starts are all sorted and we just need to compare with next
function merge_using_sort(intervals) {
    if (intervals.length < 2) {
        return intervals;
    }
    // sort the intervals on the start time
    intervals.sort((a, b) => a.start - b.start);

    const mergedIntervals = [];
    let start = intervals[0].start,
        end = intervals[0].end;
    for (i = 1; i < intervals.length; i++) {
        const interval = intervals[i];
        if (interval.start <= end) { // overlapping intervals, adjust the 'end'
            end = Math.max(interval.end, end);
        } else { // non-overlapping interval, add the previous interval and reset
            mergedIntervals.push(new Interval(start, end));
            start = interval.start;
            end = interval.end;
        }
    }
    // add the last interval
    mergedIntervals.push(new Interval(start, end));
    return mergedIntervals;
}

merged_intervals = merge([new Interval(1, 4), new Interval(2, 5), new Interval(7, 9)]);
result = "";
for (i = 0; i < merged_intervals.length; i++) {
    result += merged_intervals[i].get_interval() + " ";
}
console.log(`Merged intervals: ${result}`)

merged_intervals = merge([new Interval(6, 7), new Interval(2, 4), new Interval(5, 9)]);
result = "";
for (i = 0; i < merged_intervals.length; i++) {
    result += merged_intervals[i].get_interval() + " ";
}
console.log(`Merged intervals: ${result}`)


merged_intervals = merge([new Interval(1, 4), new Interval(2, 6), new Interval(3, 5)]);
result = "";
for (i = 0; i < merged_intervals.length; i++) {
    result += merged_intervals[i].get_interval() + " ";
}
console.log(`Merged intervals: ${result}`)