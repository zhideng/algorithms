/*
Given a binary tree and a number ‘S’, find if the tree has a path from root-to-leaf such that the sum of all the node values of that path equals ‘S’.
      [1]
      / \
    [2]  [3]
    / \  / \
  [4][5][6][7]

sum: 10
output: true
path: 1-3-6

*/

class TreeNode {

    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }
};

function has_path_util(node, paths, sum) {
    let output = false;
    // check to see if we reach the sum when we hit the leaf node
    if (node == null) {
        let pathSum = paths.reduce((a, b) => {
            return a + b;
        })

        if (pathSum == sum) {
            return true;
        } else {
            return false;
        }
    } else {
        // keep adding the children
        let currentPath = [...paths]
        currentPath.push(node.value);
        output = has_path_util(node.left, currentPath, sum);
        if (output) {
            return output;  // if we have found the output don't go any further to the rightt st
        }
        output = has_path_util(node.right, currentPath, sum);

    }
    return output;
}


const has_path = function (root, sum) {
    // TODO: Write your code here
    return has_path_util(root, [], sum);
};


var root = new TreeNode(12)
root.left = new TreeNode(7)
root.right = new TreeNode(1)
root.left.left = new TreeNode(9)
root.right.left = new TreeNode(10)
root.right.right = new TreeNode(5)
console.log(`Tree has path: ${has_path(root, 23)}`)
console.log(`Tree has path: ${has_path(root, 16)}`)