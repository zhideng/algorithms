/*
Given a binary tree and a number ‘S’, find all paths from root-to-leaf such that the sum of all the node values of each path equals ‘S’.
      [1]
      / \
    [7]  [9]
    / \  / \
  [4][5][2][7]

  sum: 12
  output: 2
  paths: [1,7,4], [1,9,2]

*/

class TreeNode {
    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }
};


const find_paths_util = function (node, paths, output, sum) {
    // let output = [];
    // check to see if we reach the sum when we hit the leaf node
    if (node.left == null && node.right == null) {
        paths.push(node.value);
        let pathSum = paths.reduce((a, b) => {
            return a + b;
        })

        if (pathSum == sum) {
            output.push(paths);
        }
    } else {
        if (node.left) {
            let currentPath = [...paths]
            currentPath.push(node.value);
            output = find_paths_util(node.left, currentPath, output, sum);
        }
        if (node.right) {
            let currentPath = [...paths]
            currentPath.push(node.value);
            output = find_paths_util(node.right, currentPath, output, sum);
        }
    }
    return output;
};

const find_paths = function (root, sum) {
    allPaths = [];
    // TODO: Write your code here
    return allPaths = find_paths_util(root, [], [], sum);
};

var root = new TreeNode(12)
root.left = new TreeNode(7)
root.right = new TreeNode(1)
root.left.left = new TreeNode(4)
root.right.left = new TreeNode(10)
root.right.right = new TreeNode(5)
sum = 23
console.log(`Tree paths with sum: ${sum}: ${find_paths(root, sum)}`)