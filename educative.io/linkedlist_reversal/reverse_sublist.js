/*
Given the head of a LinkedList and two positions ‘p’ and ‘q’, reverse the LinkedList from position ‘p’ to ‘q’.
*/

class Node {
    constructor(value, next = null) {
        this.value = value;
        this.next = next;
    }

    get_list() {
        let result = "";
        let temp = this;
        while (temp !== null) {
            result += temp.value + " ";
            temp = temp.next;
        }
        return result;
    }
};

const reverse_sub_list = function (head, p, q) {
    // TODO: Write your code here
    let prev = null;
    let current = head;
    let counter = 1;
    let front = null;
    let rear = null;
    let start_sub = null;

    while(current) {
        let tempNext = current.next;    // save the next item in a temp value

        // we hit the item we need to start reversing
        if(counter == p) {
            front = prev;   // last item in front half of list
            start_sub = current;    // this is the first item in the sub list that should be reversed
        }
        
        if(counter > p && counter <= q) {
            // reverse items
            current.next = prev;
        }

        // we hit the counter we need to stop reversing
        if(counter == q) {
            front.next = current;
            start_sub.next = tempNext;
        }
        
        // moving down the list
        prev = current; // assign prev to the current next
        current = tempNext; // advance to the next item
        counter++;
    }

    // assign the head to the last previous, the current should be null value of original list
    if(p == 1)
        head = prev;

    return head;
};


head = new Node(1)
head.next = new Node(2)
head.next.next = new Node(3)
head.next.next.next = new Node(4)
head.next.next.next.next = new Node(5)

console.log(`Nodes of original LinkedList are: ${head.get_list()}`)
console.log(`Nodes of reversed LinkedList are: ${reverse_sub_list(head, 2, 4).get_list()}`)