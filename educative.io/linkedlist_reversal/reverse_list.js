class Node {
    constructor(value, next=null){
      this.value = value;
      this.next = next;
    }
  
    get_list() {
      let result = "";
      let temp = this;
      while (temp !== null) {
        result += temp.value + " ";
        temp = temp.next;
      }
      return result;
    }
  };
  
  
  const reverse = function(head) {
    let prev = null;
    let current = head;

    while(current) {
        let tempNext = current.next;    // save the next item in a temp value
        current.next = prev;
        // moving down the list
        prev = current; // assign prev to the current next
        current = tempNext; // advance to the next item
    }

    // assign the head to the last previous, the current should be null value of original list
    head = prev;

    return head;
  };
  
  head = new Node(2);
  head.next = new Node(4);
  head.next.next = new Node(6);
  head.next.next.next = new Node(8);
  head.next.next.next.next = new Node(10);
  
  console.log(`Nodes of original LinkedList are: ${head.get_list()}`)
  console.log(`Nodes of reversed LinkedList are: ${reverse(head).get_list()}`)