/*
Given the head of a Singly LinkedList, write a function to determine if the LinkedList has a cycle in it or not.
[1]->[2]->[3]->[4]->[5]->[6]-
           ^----------------|
*/

class Node {
    constructor(value, next = null) {
        this.value = value;
        this.next = next;
    }
}

head = new Node(1)
head.next = new Node(2)
head.next.next = new Node(3)
head.next.next.next = new Node(4)
head.next.next.next.next = new Node(5)
head.next.next.next.next.next = new Node(6)

function has_cycle(node) {
    // move two pointers one 2 steps and the other 1 step, if they meet then there is cycle
    let slow = node;
    let fast = node;

    while(fast != null && fast.next != null) {
        fast = fast.next.next;  // advance 2 steps
        slow = slow.next;       // advance 1 step
        if(fast == slow) {
            return true;    // nodes are equal we have a cycle
        }
    }

    return false; // if we fall out of the loop then there is no cycle
}

// this will figure out what the length of the cycle is
function cycle_length(node) {
    // move two pointers one 2 steps and the other 1 step, if they meet then there is cycle
    let slow = node;
    let fast = node;
    let cyclePoint;

    while(fast != null && fast.next != null) {
        fast = fast.next.next;  // advance 2 steps
        slow = slow.next;       // advance 1 step
        if(fast == slow) {
            // we found the cycle point
            cyclePoint = slow;
            break;
        }
    }
    // now we move the slow one step at a time until we hit the cycle point again
    let count = 1;  // cycle has to be at least 1 step
    while(slow.next != cyclePoint) {
        count++;
        slow = slow.next;
    }

    return count;
}

console.log(has_cycle(head));
head.next.next.next.next.next.next = head.next.next;
console.log(has_cycle(head));
console.log(cycle_length(head));
head.next.next.next.next.next.next = head.next.next.next;
console.log(has_cycle(head));
console.log(cycle_length(head));