/*
Given the head of a Singly LinkedList that contains a cycle, write a function to find the starting node of the cycle.


*/

class Node {
    constructor(value, next = null) {
        this.value = value;
        this.next = next;
    }
}

const find_cycle_start = function (node) {
    // TODO: Write your code here
    // move two pointers one 2 steps and the other 1 step, if they meet then there is cycle
    let slow = node;
    let fast = node;
    let cycleLength = 0;

    while (fast != null && fast.next != null) {
        fast = fast.next.next;  // advance 2 steps
        slow = slow.next;       // advance 1 step
        if (fast == slow) {
            // nodes are equal we have a cycle
            cycleLength = cycle_length(slow);
            break;
        }
    }

    return cycle_start(node, cycleLength);

    // return head;
};

function cycle_length(node) {
    let count = 1;
    let stationaryNode = node;
    let movingNode = node;

    while (movingNode.next != stationaryNode) {
        count++;
        movingNode = movingNode.next;
    }

    return count;
}

function cycle_start(node, cycle_length) {
    let fast = node;
    let slow = node;

    // move the fast the length of the cycle
    for (let i = 0; i < cycle_length; i++) {
        fast = fast.next;
    }

    // now move fast and slow by 1 step until they meet
    while(fast != slow) {
        slow = slow.next;
        fast =fast.next;
    }

    return slow;
}

head = new Node(1)
head.next = new Node(2)
head.next.next = new Node(3)
head.next.next.next = new Node(4)
head.next.next.next.next = new Node(5)
head.next.next.next.next.next = new Node(6)

head.next.next.next.next.next.next = head.next.next
console.log(`LinkedList cycle start: ${find_cycle_start(head).value}`)

head.next.next.next.next.next.next = head.next.next.next
console.log(`LinkedList cycle start: ${find_cycle_start(head).value}`)

head.next.next.next.next.next.next = head
console.log(`LinkedList cycle start: ${find_cycle_start(head).value}`)