/*
We are given an array containing ‘n’ distinct numbers taken from the range 0 to ‘n’. Since the array has only ‘n’ numbers out of the total ‘n+1’ numbers, find the missing number.

Input: [4, 0, 3, 1]
Output: 2

Input: [8, 3, 5, 2, 4, 6, 0, 1]
Output: 7

this should be the same as the cyclic sort but check for zero number and move on if it is
*/

function find_missing_number(nums) {
    let index = 0;  // so the solution is to move one at a time and swap to the correct index value until we reach the end of the array

    while(index < nums.length) {
        if(nums[index] != index + 1 && nums[index] != 0) {
            // its out of place so swap
            let temp = nums[nums[index]-1];
            nums[nums[index]-1] = nums[index];
            nums[index] = temp;
        } else {
            // its in the right place so move the index
            index++;
        }
    }

    // iterate through the sorted array and return the value that doesn't match the index
    for(let i = 0; i < nums.length; i++) {
        if(nums[i] != i + 1)
            return i + 1;
    }
}


console.log(find_missing_number([4, 0, 3, 1]));
console.log(find_missing_number([8, 3, 5, 2, 4, 6, 0, 1]));