/*
We are given an array containing ‘n’ objects. Each object, when created, was assigned a unique number from 1 to ‘n’ based on their creation sequence. This means that the object with sequence number ‘3’ was created just before the object with sequence number ‘4’.

Write a function to sort the objects in-place on their creation sequence number in O(n)O(n) and without any extra space. For simplicity, let’s assume we are passed an integer array containing only the sequence numbers, though each number is actually an object.

Input: [3, 1, 5, 4, 2]
Output: [1, 2, 3, 4, 5]

Input: [2, 6, 4, 3, 1, 5]
Output: [1, 2, 3, 4, 5, 6]

Input: [1, 5, 6, 4, 3, 2]
Output: [1, 2, 3, 4, 5, 6]

we need to do this in place, I was just going to make an array the same size and go through each and put them in correct index
the Big O would be O(n) but space complexity would be 2N
*/
function cyclic_sort(nums) {
    let index = 0;  // so the solution is to move one at a time and swap to the correct index value until we reach the end of the array

    while(index < nums.length) {
        if(nums[index] != index + 1) {
            // its out of place so swap
            let temp = nums[nums[index]-1];
            nums[nums[index]-1] = nums[index];
            nums[index] = temp;
        } else {
            // its in the right place so move the index
            index++;
        }
    }

    return nums;
}


console.log(cyclic_sort([3, 1, 5, 4, 2]));
console.log(cyclic_sort([2, 6, 4, 3, 1, 5]));
console.log(cyclic_sort([1, 5, 6, 4, 3, 2]));