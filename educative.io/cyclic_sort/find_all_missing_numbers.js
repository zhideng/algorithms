/*
We are given an unsorted array containing numbers taken from the range 1 to ‘n’. The array can have duplicates, which means some numbers will be missing. Find all those missing numbers.

Input: [2, 3, 1, 8, 2, 3, 5, 1]
Output: 4, 6, 7
Explanation: The array should have all numbers from 1 to 8, due to duplicates 4, 6, and 7 are missing.

Input: [2, 4, 1, 2]
Output: 3

Input: [2, 3, 2, 1]
Output: 4

this really is the same as the cyclic sort with 1 missing number, when we encounter a duplicate we just replace with zero
*/

// my solution has flaws and will go into infinite loop when it continues to interate and cant move on
function find_missing_numbers(nums) {
    let index = 0;  // so the solution is to move one at a time and swap to the correct index value until we reach the end of the array
    let last_sorted_value = 0;
    let missing_values = [];
    while (index < nums.length) {

        if (nums[index] <= last_sorted_value) {
            // the number that exists is already in position (duplicate)
            // replace with zero and move index
            nums[index] = 0;
            index++;
        } else if (nums[index] != index + 1) {
            // its out of place so swap
            let temp = nums[nums[index] - 1];
            nums[nums[index] - 1] = nums[index];
            nums[index] = temp;
        } else {
            // its in the right place so move the index
            last_sorted_value = nums[index];
            index++;
        }
    }

    for (let i = 0; i < nums.length; i++) {
        if (nums[i] != i + 1) {
            missing_values.push(i + 1);
        }
    }

    return missing_values;
}

// answer from internet, mine has infinite loop issue
function find_missing_numbers2(nums) {
    let i = 0;
    while (i < nums.length) {
        const j = nums[i] - 1;  //**THIS IS THE KEY, j is the index position that we are looking for so if the current value is not it then we need to swap */
        if (nums[i] !== nums[j]) {
            [nums[i], nums[j]] = [nums[j], nums[i]]; // swap
        } else {
            i += 1;
        }
    }
    missingNumbers = [];

    for (i = 0; i < nums.length; i++) {
        if (nums[i] !== i + 1) {
            missingNumbers.push(i + 1);
        }
    }

    return missingNumbers;
}


console.log(find_missing_numbers([2, 3, 1, 8, 2, 3, 5, 1]));
console.log(find_missing_numbers2([2, 4, 1, 2]));
console.log(find_missing_numbers2([2, 3, 2, 1]));