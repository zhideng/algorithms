/*
Given an array of lowercase letters sorted in ascending order, find the smallest letter in the given array greater than a given ‘key’.

Assume the given array is a circular list, which means that the last letter is assumed to be connected with the first letter. This also means that the smallest letter in the given array is greater than the last letter of the array and is also the first letter of the array.

Write a function to return the next letter of the given ‘key’.

Input: ['a', 'c', 'f', 'h'], key = 'f'
Output: 'h'
Explanation: The smallest letter greater than 'f' is 'h' in the given array.

Input: ['a', 'c', 'f', 'h'], key = 'b'
Output: 'c'
Explanation: The smallest letter greater than 'b' is 'c'.

Input: ['a', 'c', 'f', 'h'], key = 'm'
Output: 'a'
Explanation: As the array is assumed to be circular, the smallest letter greater than 'm' is 'a'.

Input: ['a', 'c', 'f', 'h'], key = 'h'
Output: 'a'
Explanation: As the array is assumed to be circular, the smallest letter greater than 'h' is 'a'.
*/

const search_next_letter = function (letters, key) {
    let start = 0;
    end = letters.length - 1;
    isAscending = letters[start] < letters[end];
    while (start <= end) {
        // calculate the middle of the current range
        mid = Math.floor(start + (end - start) / 2);

        if (key === letters[mid]) {
            // we found a match pick the next one
            if (mid + 2 > letters.length) {
                // if we are at the last letter in the array
                return letters[0];
            } else {
                return letters[mid + 1];
            }
        } else {
            if (start == end) {
                // so we reached the ending and we can't find the letter
                if (key > letters[start]) {
                    // if its still greater then we reached the end of the array return the fisrt
                    if (end == letters.length - 1) {
                        return letters[0];
                    } else {
                        return letters[start + 1];
                    }

                } else {
                    return letters[start];
                }
            }
        }

        if (key < letters[mid]) {
            end = mid - 1; // the 'key' can be in the first half
        } else { // key > arr[mid]
            start = mid + 1; // the 'key' can be in the second half
        }
    }

    return letters[0]; // if we reach the end then we return the first
};

// this is so much better approach than the conditional checks
function search_next_letter_answer(letters, key) {
    const n = letters.length;
    if (key < letters[0] || key > letters[n - 1]) {
      return letters[0];
    }
  
    let start = 0;
    let end = n - 1;
    while (start <= end) {
      mid = Math.floor(start + (end - start) / 2);
      if (key < letters[mid]) {
        end = mid - 1;
      } else { // key >= letters[mid]:
        start = mid + 1;
      }
    }
    // since the loop is running until 'start <= end', so at the end of the while loop, 'start === end+1'
    return letters[start % n];
  }

console.log(search_next_letter(['a', 'c', 'f', 'h'], 'f'))
console.log(search_next_letter(['a', 'c', 'f', 'h'], 'b'))
console.log(search_next_letter(['a', 'c', 'f', 'h'], 'm'))