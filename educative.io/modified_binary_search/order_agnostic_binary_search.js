/*
Given a sorted array of numbers, find if a given number ‘key’ is present in the array. Though we know that the array is sorted, we don’t know if it’s sorted in ascending or descending order. You should assume that the array can have duplicates.

Write a function to return the index of the ‘key’ if it is present in the array, otherwise return -1.

Input: [4, 6, 10], key = 10
Output: 2

Input: [1, 2, 3, 4, 5, 6, 7], key = 5
Output: 4

Input: [10, 6, 4], key = 10
Output: 0

Input: [10, 6, 4], key = 4
Output: 2
*/

// this is my solution using recursion but the answer could have been done using loops
function binary_search_util(arr, key, index_end) {
    if (arr.length == 0) {
        return -1;
    }

    if (arr.length == 1 && arr[0] == key) {
        return index_end;
    }

    let ascending = true;
    if (arr[0] > arr[arr.length - 1]) {
        ascending = false;
    }

    // split the array in half
    let midpoint = Math.floor(arr.length / 2);   // if odd number include in first half
    if (arr[midpoint] == key) {
        return index_end - (arr.length - 1 - midpoint); //how far away are we from the end index
    }
    if (ascending) {
        if (key > arr[midpoint]) {
            return binary_search_util(arr.slice(midpoint + 1, arr.length), key, index_end);
        } else {
            return binary_search_util(arr.slice(0, midpoint), key, index_end - (midpoint + 1));
        }
    } else {
        if (key > arr[midpoint]) {
            return binary_search_util(arr.slice(0, midpoint), key, index_end - (midpoint + 1));
        } else {
            return binary_search_util(arr.slice(midpoint + 1, arr.length), key, index_end);
        }
    }

    return -1;
}

const binary_search = function (arr, key) {
    return binary_search_util(arr, key, arr.length - 1);
};

// answer from internet, using loops is much simpler
function binary_search_loop(arr, key) {
    let start = 0;
    end = arr.length - 1;
    isAscending = arr[start] < arr[end];
    while (start <= end) {
        // calculate the middle of the current range
        mid = Math.floor(start + (end - start) / 2);

        if (key === arr[mid]) {
            return mid;
        }
        if (isAscending) { // ascending order
            if (key < arr[mid]) {
                end = mid - 1; // the 'key' can be in the first half
            } else { // key > arr[mid]
                start = mid + 1; // the 'key' can be in the second half
            }
        } else { // descending order
            if (key > arr[mid]) {
                end = mid - 1; // the 'key' can be in the first half
            } else { // key < arr[mid]
                start = mid + 1; // the 'key' can be in the second half
            }
        }
    }

    return -1; // element not found
}

console.log(binary_search([4, 6, 10], 6))
console.log(binary_search([1, 2, 3, 4, 5, 6, 7], 5))
console.log(binary_search_loop([1, 2, 3, 4, 5, 6, 7], 5))
console.log(binary_search([10, 6, 4], 10))
console.log(binary_search([10, 6, 4], 4))