// https://www.hackerrank.com/challenges/staircase/problem
function staircase(n) {
    for (let i = 1; i < n+1; i++) {
        let line ="";
        for(let j = 1; j < n+1; j++) {
            
            if(i+j >= n+1) {
                line += "#";
            } else {
                line += " ";
            }
        }
        console.log(line);
    }
}

staircase(4)