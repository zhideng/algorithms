// https://leetcode.com/problems/combination-sum/

function combinations(values, target) {
    let results = [];
    combinationUtil(values, [], target, results);
    return results;
}

function combinationUtil(values, current, target, results) {
    // check to see if there are values
    if (values.length == 0) {
        return values;
    }

    if (target == 0) {
        results.push(current);
        return;
    }

    for (let i = 0; i < values.length; i++) {
        // we need to check that the values are in increasing order or else we will get duplicaates
        if (current.length == 0 || current[current.length - 1] <= values[i]) {
            if (target - values[i] >= 0) {
                let arr = JSON.parse(JSON.stringify(current));
                arr.push(values[i]);
                combinationUtil(values, arr, target - values[i], results);
            }
        }
    }
}

let values = [2,3,5];

console.log(combinations(values, 8));