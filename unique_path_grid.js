// https://www.interviewbit.com/problems/unique-paths-in-a-grid/

function find_paths(A, x, y) {
    let count = 0;

    // if the next position is not valid(out of range or matrix position not allowed) then return 0
    if(A[x][y] == 1) {
        return 0
    }

    // we have hit the end so return a count
    if(x == A.length -1 && y == A[0].length - 1 && A[x][y] != 1) {
        return 1
    }

    // attempt to go (x+1, y) and (x,y+1) position
    if(x+1 <= A.length - 1) {
        count += find_paths(A, x+1, y);
    }
    if(y+1 <= A[0].length - 1) {
        count += find_paths(A, x, y+1);
    }
    
    return count;
}

function unqiue_paths(A) {
    // strat at 0,0 and then try and find n x m location
    let count = 0;
    // starting position is x = 0, y = 0
    count += find_paths(A, 0, 0);

    return count;
}

let matrix = [
    [0,0,0],
    [0,1,0],
    [0,0,0]
]

// let matrix = [
//     [0,1]
// ]

console.log(unqiue_paths(matrix));