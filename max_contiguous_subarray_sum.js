// https://www.interviewbit.com/problems/max-sum-contiguous-subarray/

// this is the brute force way and will go through basically all combinations
function maxSum(A) {
    if (A.length == 0) {
        return 0;
    }
    // lets do the dumb way which is to go through all combinations and find the max sum
    let maxSum = A[0];
    // let start_index = 0;
    for (let i = 0; i < A.length; i++) {
        let sum = A[i];
        if (sum > maxSum) {
            maxSum = sum;
        }
        for (let k = i + 1; k < A.length; k++) {
            sum += A[k];
            if (sum > maxSum) {
                maxSum = sum;
            }
        }
    }
    return maxSum;
}

// kadane's algo found here https://www.geeksforgeeks.org/largest-sum-contiguous-subarray/
function maxSum_Kadane(A) {
    let max_so_far = A[0];
    let max_here = 0;

    for (let i = 0; i < A.length; i++) {
        max_here = max_here + A[i];

        if (max_so_far < max_here) {
            max_so_far = max_here
        }

        if (max_here < 0) {
            max_here = 0;
        }
    }

    return max_so_far;
}

let arr = [-2, 1, -3, 4, -1, 2, 1, -5, 4]
console.log(maxSum_Kadane(arr));