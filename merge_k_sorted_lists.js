// https://leetcode.com/problems/merge-k-sorted-lists/

// I'm not going to do the dumb way and put them all into an array and quick sort it
// this will go through each list and each and try and place the item where it belongs hoping for O(kN)
// where k is the number of lists and N is the number of nodes

function ListNode(val) {
    this.val = val;
    this.next = null;
}

function printList(list) {
    let node = list
    while(node) {
        console.log(node.val);
        node = node.next;
    }
}

let mergeKLists = function(lists) {
    // check that we don't have empty lists
    if(lists.length === 0) {
        return lists;
    }
    let output = null;
    let startCounter = 0;
    // well take the first non empty list
    for(let i = 0; i < lists.length; i++) {
        if(lists[i] != null) {
            output = lists[i];
            startCounter = i;
            break;
        }
    }
    // let output = lists[0];
    let currentPosition = lists[0];
    let prevPosition = null;
    // go through each remaining list and each item and insert into the reference list that we picked
    for(let i = startCounter + 1; i < lists.length; i++) {
        currentPosition = lists[i-1];
        let listNode = lists[i];
        while(listNode) {
            let tempNode = listNode.next;
            if(currentPosition == null) {
                // this means that the value we are trying to insert is larger than all values in the list and add to the end
                if(prevPosition) {
                    prevPosition.next = listNode;
                } else {
                    // the means there was no previous and we are at the start
                    currentPosition = listNode;
                }
                // move to next item
                listNode = tempNode;
            } else if(currentPosition.val >= listNode.val) {
                // insert in front and move the postion to the the inserted element
                listNode.next = currentPosition;
                currentPosition = listNode;
                if(prevPosition) {
                    prevPosition.next = currentPosition;
                }
                //only move the output if it is larger than the value inserted
                if(output.val >= listNode.val) {
                    output = listNode;
                }
                // move to the next node
                listNode = tempNode;
            } else {
                // move the current position
                prevPosition = currentPosition;
                currentPosition = currentPosition.next;
            }
        }
    }
    return output;
};

let list1 = new ListNode(1);
list1.next = new ListNode(4);
list1.next.next = new ListNode(5);

// list1 = null;

let list2 = new ListNode(1);
list2.next = new ListNode(3);
list2.next.next = new ListNode(4);

let list3 = new ListNode(2);
list3.next = new ListNode(6);

let input = [list1, list2, list3];

printList(mergeKLists(input));