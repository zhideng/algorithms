// https://www.interviewbit.com/problems/reverse-link-list-ii/

var LinkList = function (value) {
    return {
        value: value,
        next: null
    }
}

function PrintList(L) {
    let node = L;
    while (node) {
        console.log(node.value);
        node = node.next;
    }
}


function reverse_list(L, start, end) {
    if (!L || start >= end || start == 0) {
        return L;
    }

    let counter = 1;
    let currentNode = L;
    let previousNode;
    let startReverseNode;
    let reversedNode;
    let firstStartNode;
    while (currentNode) {
        let tempCurrent = currentNode;
        // determine if we are at the position where we want to start reversing
        if (counter == start) {
            startReverseNode = previousNode;
            reversedNode = new LinkList(currentNode.value);
            firstStartNode = reversedNode;
        } else if (counter > start && counter <= end) {
            previousNode.next = currentNode.next;
            let tempNode = new LinkList(currentNode.value);
            tempNode.next = reversedNode;
            reversedNode = tempNode;
        } 

        if (counter == end || tempCurrent.next == null) {
            if (startReverseNode) {
                startReverseNode.next = reversedNode;
                firstStartNode.next = currentNode.next;
            } else {
                // the start must have been 1 so there is no startReverseNode
                // need to check if its the full list, if the current node's next value is null it means we reached the end
                // and we need to point L as the entire reversed list
                if (currentNode.next) {
                    firstStartNode.next = currentNode.next;
                }
                L = reversedNode;

            }
            break;
        }
        previousNode = tempCurrent;
        currentNode = tempCurrent.next;
        counter++;
    }

    return L;
}

let node = new LinkList(1);
node.next = new LinkList(2);
node.next.next = new LinkList(3);
node.next.next.next = new LinkList(4);
node.next.next.next.next = new LinkList(5);
node.next.next.next.next.next = new LinkList(6);

PrintList(reverse_list(node, 1, 5));

