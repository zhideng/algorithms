// https://www.hackerrank.com/challenges/mini-max-sum/problem
function min_max(arr) {
    let sum = 0;
    // add up all the numbers first and set as max and min values
    for(let i = 0; i < arr.length; i++) {
        sum += arr[i];
    }
    let min = sum;
    let max = 0;
    for(let i = 0; i < arr.length; i ++) {
        //subtract the value from the sum and update min and max accordingly
        let newValue = sum - arr[i]
        if(newValue < min) {
            min = newValue
        }
        if(newValue > max) {
            max = newValue
        }
    }
    console.log(`${min} ${max}`);
}

min_max([1,2,3,4,5,])