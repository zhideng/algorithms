// https://www.geeksforgeeks.org/largest-subset-whose-all-elements-are-fibonacci-numbers/

function fibonacci_finder(arr) {
    let output = [];
    let lookup = [0,1,1];    // this is a look up to see if the number is fibonacci, initialize with 3 values
    // loop through each number in the array
    for(let i = 0; i < arr.length; i++) {
        let number = arr[i];
        // check the last number in the lookup is larger than current number
        if(lookup[lookup.length -1] <= number) {
            // if there is no look up or the largest value in the look up (last value) is not present continue generating fibonacci numbers
            while(lookup[lookup.length - 1] < number) {
                lookup.push(lookup[lookup.length -1] + lookup[lookup.length - 2]);
            }
        }
        // check to see if number is in lookup
        if(lookup.includes(number)) {
            output.push(number);
        }
    }
    return output;
}

let items = [4, 2, 8, 5, 20, 1, 40, 13, 23];
console.log(fibonacci_finder(items));