// https://leetcode.com/problems/generate-parentheses/

// this will be the dumb way and generate all possible combinations of open close and checking if they are valid
function bruteForce(n) {
    let results = [];
    generateAll(n * 2, [], results);
    return results;
}

function generateAll(n, current, results) {
    if (current.length == n) {
        if (validSeries(current)) {
            results.push(current.join(''));
        }
    } else {
        let open = JSON.parse(JSON.stringify(current));;
        let close = JSON.parse(JSON.stringify(current));;
        open.push('(');
        close.push(')');
        generateAll(n, open, results);
        generateAll(n, close, results);
    }
}

function validSeries(n) {
    // go through each item and make sure that the close tags never try and close when there is no open
    let balance = 0;
    for (let i = 0; i < n.length; i++) {
        if (n[i] == '(') {
            balance++;
        } else {
            balance--;
        }
        if (balance < 0) {
            // we can never have a negative because that would mean too many close tags
            return false;
        }
    }
    if (balance == 0) {
        return true;
    } else {
        return false;
    }
}

// modifying the bruteForce to check to see if we have enough open tags before we add a close so we don't have to validate each potential series
function generateAllBackTrack(n, current, openCount, closeCount, results) {
    if (current.length == n) {
        results.push(current.join(''));
    } else {
        if (openCount < n / 2) {
            let open = JSON.parse(JSON.stringify(current));
            open.push('(');
            generateAllBackTrack(n, open, openCount + 1, closeCount, results);
        }
        if (closeCount < openCount) {
            let close = JSON.parse(JSON.stringify(current));
            close.push(')');
            generateAllBackTrack(n, close, openCount, closeCount+1, results);
        }
    }
}

function backTrack(n) {
    let results = [];
    generateAllBackTrack(n * 2, [], 0, 0, results);
    return results;
}

console.log(backTrack(3));