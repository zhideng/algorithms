// https://www.interviewbit.com/problems/reverse-integer/

function reverse(value) {
    let reversedNum = 0;
    let isNegative = value < 0 ? true : false;
    // convert back to positive
    if(isNegative) {
        value = value * -1;
    }
    while(value != 0) {
        reversedNum = reversedNum * 10;
        reversedNum = reversedNum + value % 10;
        value = Math.floor(value / 10);
    }
    if(reversedNum >= 2147483647) {
        return 0;
    }

    if(isNegative) {
        reversedNum = reversedNum * -1;
    }
    return reversedNum;

}

console.log(reverse(-1));