// https://www.geeksforgeeks.org/sum-nodes-longest-path-root-leaf-node/
// this question was asked by fb but instead of of sum they wanted the actual path
// modifying this to return the longest path

var Node = function(data) {
    return {
        data: data,
        left: null,
        right: null
    }
}

// wrapper object to hold both the path and the Sum
var PathNSum = function() {
    return {
        sum: 0,
        path: []
    }
}

/*
sample tree
      4
    /   \
   2     5
  / \   / \
 8   1 2   3
    /
   6
the longest path should be 4-2-1-6
*/
var root = new Node(4);  
root.left = new Node(2); 
root.right = new Node(5); 
root.left.left = new Node(8); 
root.left.right = new Node(1); 
root.right.left = new Node(2); 
root.right.right = new Node(3); 
root.left.right.left = new Node(6); 

// this will find the longest path
function FindLngestPathUtil(node, path, maxPath) {

    // if we reached the leaf node then check to see if we have the longest path
    if(node == null) {
        if(path.length > maxPath.length) {
            maxPath = path
        }
        return maxPath;
    } else {
        // add node into the path
        path.push(node.data);
    }

    let localPathLeft = Array.from(path);
    let localPathRight = Array.from(path);

    maxPath = FindLngestPathUtil(node.left, localPathLeft, maxPath);
    maxPath = FindLngestPathUtil(node.right, localPathRight, maxPath);

    return maxPath;
}

function FindMaxSumUtil(node, curr, max) {
    if(node == null) {
        if(curr.sum > max.sum) {
           max = JSON.parse(JSON.stringify(curr));;
        }
        return max;
    } else {
        curr.sum += node.data;
        curr.path.push(node.data);
    }

    // make a copy of the current so that we capture the value at the point in time of recursive call
    let currentLeft = JSON.parse(JSON.stringify(curr));
    let currentright = JSON.parse(JSON.stringify(curr))

    max = FindMaxSumUtil(node.left, currentLeft, max);
    max = FindMaxSumUtil(node.right, currentright, max);

    return max;
}

// entry function for longest path
function FindLongestPath(root) {
    
    return FindLngestPathUtil(root, [], []);

}

// now we can solve for sum of longest path by just adding all the values of the longest path
function SumLongestPath(root) {
    let maxPath = FindLngestPathUtil(root, [], []);

    return maxPath.reduce((prev, curr) => {
        return prev + curr;
    })
}

// we can also modify this to return the max sum of any path
// instead of tracking the path we just track the sum
function MaxPathSum(root) {
    let curr = new PathNSum();
    let max = new PathNSum();
    max = FindMaxSumUtil(root, curr, max);
    return max.sum;
}

function MaxPathWithLargestSum(root) {
    let curr = new PathNSum();
    let max = new PathNSum();
    max = FindMaxSumUtil(root, curr, max);
    return max.path;
}

console.log(FindLongestPath(root));

console.log(SumLongestPath(root));

console.log(MaxPathSum(root));

console.log(MaxPathWithLargestSum(root));