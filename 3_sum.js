// https://www.interviewbit.com/problems/3-sum/
// use strategies from here https://www.geeksforgeeks.org/find-a-triplet-that-sum-to-a-given-value/

function closest_sum_naive(A, B) {
    let difference = null;
    let total = 0;
    // go through all combinations
    for (let i = 0; i < A.length - 2; i++) {
        for (let j = i + 1; j < A.length - 1; j++) {
            for (let k = j + 1; k < A.length; k++) {
                var sum = A[i] + A[j] + A[k];
                if (difference == null) {
                    difference = sum - B
                    total = sum;
                } else if (Math.abs(sum - B) < Math.abs(difference)) {
                    difference = sum - B;
                    total = sum;
                }
            }
        }
    }

    return total;
}

// sort the array then start at the beginning and use two pointers(one at beginning and one at end) and move towards each other until you get to sum
function closest_sum_sorted(A, B) {
    let sorted = A.sort(function (a, b) { return a - b });
    let total = 0;
    let difference = null;
    for (let i = 0; i < sorted.length - 2; i++) {
        let l = i + 1;
        let r = A.length - 1;
        while (l < r) {
            sum = sorted[i] + sorted[l] + sorted[r];
            if (sum == B) {
                return sum;
            } else if (difference == null) {
                difference = sum - B;
                total = sum;
            } else if (Math.abs(sum - B) < Math.abs(difference)) {
                difference = sum - B;
                total = sum;
            }

            //move the pointer
            if (sum < B) {
                l++
            } else {
                r--;
            }
        }
    }
    return total;
}

let arr = [7, -6, 2, 10]

//console.log(closest_sum_sorted(arr, 3));

// slightly different problem than the above but similar strategy
// https://leetcode.com/problems/3sum/

function threeSum(n) {
    // using the same strategy (not brute force) get all the values that add up to zero (hard coded sum)
    // sort the array
    let sorted = n.sort((prev, curr) => {
        return prev - curr;
    })
    let prevStartVal = null;
    let output = [];
    for (let i = 0; i < sorted.length - 2; i++) {
        if (prevStartVal != sorted[i]) {
            let l = i + 1;
            let r = sorted.length - 1
            while (r > l) {
                let sum = sorted[i] + sorted[l] + sorted[r];
                if (sum == 0) {
                    output.push([sorted[i], sorted[l], sorted[r]]);
                    // move the left pointer
                    if (sorted[l] == sorted[l + 1]) {
                        while (r > l && sorted[l] == sorted[l + 1]) {
                            l++;
                        }
                        l++;
                    } else {
                        l++;
                    }
                } else if (sum > 0) {
                    // we don't want duplicates so we need to move the pointer until it's a different value
                    if (sorted[r] == sorted[r - 1]) {
                        while (r > l && sorted[r] == sorted[r - 1]) {
                            r--;
                        }
                        r--;
                    } else {
                        r--;
                    }
                } else {
                    // we don't want duplicates so we need to move the pointer until it's a different value
                    if (sorted[l] == sorted[l + 1]) {
                        while (r > l && sorted[l] == sorted[l + 1]) {
                            l++;
                        }
                        l++;
                    } else {
                        l++;
                    }
                }
            }
        }
        prevStartVal = sorted[i];
    }
    return output;
}

let values = [-1, 0, 1, 2, -1, -4];
console.log(threeSum(values));