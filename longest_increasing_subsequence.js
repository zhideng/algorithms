// https://www.interviewbit.com/problems/longest-increasing-subsequence/
// great explanation here: https://www.youtube.com/watch?v=CE2b_-XfVDk

function longest_sequnce(A) {

    // check for empty array and single value array
    if(A.length == 0)
        return 0;

    if(A.length == 1)
        return 1

    // create an array that is the size of the input, at each position we will try and find the largest increasing sub array
    let sequences = new Array(A.length);

    for(let i = 0; i < sequences.length; i++) {
        sequences[i] = 1;
    }

    // initialize the arrays to all 1's since that is the min that we can have
    for(let i = 1; i < A.length; i++) {
        for(let j = 0; j < i; j ++) {
            if(A[j] < A[i]) {
                sequences[i] = Math.max(sequences[i], sequences[j] + 1);
            }
        }
    }

    return sequences.reduce((a,b) => {
        return Math.max(a,b);
    })
}

let arr = [0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15];

console.log(longest_sequnce(arr));
