// https://www.interviewbit.com/problems/ways-to-decode/

function get_numways(arr) {
    let count = 0;
    if(arr.length <= 1) {
        return 1
    }

    // we need to recursively check if n-1 and n-2 can also be decoded
    count += get_numways(arr.slice(0, arr.length - 1))

    if(arr.length >= 2 && ((arr[arr.length - 2] == 2 && arr[arr.length - 1] < 7) || arr[arr.length - 2] < 2 )) {
        count += get_numways(arr.slice(0, arr.length - 2))
    }

    return count;
}

// this works but is O(n^n)
function num_ways(A) {
    let count = 0;
    // take the integer value and split into array of digits
    let digits = A.split("");

    count = get_numways(digits);

    return count;
}

function split_number(A) {
    if(A < 10) {
        return [A];
    }
    let arr = [];
    while(A > 0) {
        let digit = A % 10;
        arr.unshift(digit);
        A = Math.floor(A/10);
    }
    return arr;
}

function num_ways_dp(A) {
    let arr = A.split("");

    if(arr[0] == "0") {
        return 0;
    }

    let count = new Array(arr.length + 1);
    count[0] = 1;
    count[1] = 1;

    for(let i = 2; i <= arr.length; i++) {
        count[i] = 0

        if(arr[i - 1] > "0") {
            count[i] = count[i - 1];
        }

        if(arr[i - 2] == "1" || (arr[i - 2] == "2" && arr[i - 1] < "7")) {
            count[i] += count[i - 2];
        }
    }
    return count[arr.length];

}

console.log(num_ways_dp("123"));