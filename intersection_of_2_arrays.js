// https://www.interviewbit.com/problems/intersection-of-sorted-arrays/

function intersection(A, B) {
    // we will create pointers for each array and store a output of intersections
    // move the pointer for each array until either both reach the end or the remaining pointer has a larger array value item
    let a_index = 0;
    let b_index = 0;
    let output = [];

    while(a_index <= A.length - 1 && b_index <= B.length - 1) {
        // check to see if the values are the same
        if(A[a_index] == B[b_index]) {
            output.push(A[a_index]);
            // increment both
            a_index++;
            b_index++;
        } else if( A[a_index] < B[b_index]) {
            // see if there is anymore places to move A
            if(a_index + 1 <= A.length - 1) {
                a_index++;
            } else {
                break; // we reached the end of A so no need to go any further
            }
        } else if(A[a_index] > B[b_index]) {
            if(b_index + 1 <= B.length - 1) {
                b_index++;
            } else {
                break; // we reached the end of B so no ned to go any further;
            }
        }
        
    }
    return output;
}

let arr1 = [1,2,3,3,4,5,6];
let arr2 = [3, 5];

console.log(intersection(arr1, arr2))