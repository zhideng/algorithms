// problem I was given at facebook, given an integer return the alpha representation of it
// i.e. 410,456,931 outputs four hundred ten million four hundred fifty six thousand nine hundred thirty one

let ones = {
    1: "one",
    2: "two",
    3: "three",
    4: "four",
    5: "five",
    6: "six",
    7: "seven",
    8: "eight",
    9: "nine"
}

let tens = {
    10: "ten",
    11: "eleven",
    12: "twelve",
    13: "thirteen",
    14: "fourteen",
    15: "fifteen",
    16: "sixteen",
    17: "seventeen",
    18: "eighteen",
    19: "nineteen"
}

let tens2 = {
    2: "twenty",
    3: "thirty",
    4: "fourty",
    5: "fifty",
    6: "sixty",
    7: "seventy",
    8: "eighty",
    9: "ninety"
}

let all = {
    1: "thousand",
    2: "million",
    3: "billion",
}

function alphaNum(n) {
    if(n == 0) {
        return "zero";
    }

    let output = [];
    let counter = 1; // this counts how deep you are in the number
    let prevNum = 0;
    let largeValueCounter = 1;
    counter = 1;

    while(n > 0) {

        let value = n % 10;

        let counterCase = counter % 4;
        let onesValue = null;

        switch (counterCase) {
            case 1:
                onesValue = ones[value]
                if(onesValue) {
                    output.unshift(ones[value])
                }
                break;
            case 2:
                if(value == 1) {
                    // this means its a inbetween number, remove the previous and replace with this
                    let checkNum = value * 10 + prevNum;
                    if(prevNum > 0)
                        output.shift(); // only remove if the value is not zero
                    output.unshift(tens[checkNum]);
                } else {
                    let test = tens2[value];
                    output.unshift(tens2[value]);
                }
                break;
            case 3:
                // everytime we hit this we need to get the hudred value
                output.unshift("hundred");
                output.unshift(ones[value]);
                break;
            case 0:
                // everytime we hit this we need to get the large number value
                output.unshift(all[largeValueCounter]);
                onesValue = ones[value]
                if(onesValue) {
                    output.unshift(ones[value])
                }
                largeValueCounter++;
                counter++;
                break;
        }
        counter++;
        prevNum = value;
        n = Math.floor(n / 10);
    }

    return output.join(" ");
}

console.log(alphaNum(410456931));
console.log(alphaNum(1921));
console.log(alphaNum(19));
console.log(alphaNum(80));
console.log(alphaNum(42));