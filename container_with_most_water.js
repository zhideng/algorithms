// https://leetcode.com/problems/container-with-most-water/solution/

function bruteForce(n) {
    let maxArea = 0
    for(let i = 0; i < n.length-1; i++) {
        for(let j = i + 1; j < n.length; j++) {
            let length = j - i;
            let height = Math.min(n[i], n[j]);
            maxArea = Math.max(maxArea, length * height);
        }
    }
    return maxArea;
}

function two_pointer(n) {
    let maxArea = 0;
    let l = 0;
    let r = n.length - 1;
    // so we will calculate from both ends and move the smaller height towards the other end
    while(r > l) {
        let length = r - l;
        let height = Math.min(n[l], n[r]);
        maxArea = Math.max(maxArea, length * height);
        if(n[l] <= n[r]) {
            l++;
        } else {
            r--;
        }
    }
    return maxArea;
}

let values = [2,3,4,5,18,17,6];

console.log(two_pointer(values));