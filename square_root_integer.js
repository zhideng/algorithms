// https://www.interviewbit.com/problems/square-root-of-integer/

function sq_root(n) {

    // we will try and find the square root but tesing from 1 - n/2
    // we will use binary search to try and pinpoint quickly where in the range that number lies

    let start = 1
    let end = Math.round(n/2);
    let result = 0;

    while(start <= end) {
        let mid = Math.round((start + end) / 2);
        let sqrt = mid*mid;

        // check to see if the result has been reached
        if(sqrt == n) {
            return mid;
        } else if( sqrt < n) {
            result = mid;   // set the result to the last mid we encountered
            // this means our value is in the larger half
            start = mid + 1;
        } else {
            // that means that the value is somewhere in the smaller
            end = mid - 1;
        }
    }

    return result;
}

console.log(sq_root(15));